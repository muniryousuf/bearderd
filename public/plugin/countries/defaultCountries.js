var input = document.querySelector("#input-userPhone"),
errorMsg = document.querySelector("#error-msg"),
validMsg = document.querySelector("#valid-msg");

// here, the index maps to the error code returned from getValidationError - see readme
var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

// initialise plugin
var iti = window.intlTelInput(input, {
	hiddenInput: "full_phone",
	initialCountry: "auto",
	// onlyCountries: ['pk'], // if you wanna use Only 1 Country
	geoIpLookup: function(callback) {
		$.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
		var countryCode = (resp && resp.country) ? resp.country : "";
			callback(countryCode);
		});
	},
	utilsScript: themeassets
});

var reset = function() {
	input.classList.remove("error");
	errorMsg.innerHTML = "";
	errorMsg.classList.add("d-none");
	validMsg.classList.add("d-none");
};

// on blur: validate
input.addEventListener('keyup', function() {
	reset();
	if (input.value.trim()) {
		if (iti.isValidNumber()) {
			validMsg.classList.remove("d-none");
			phoneVal = true;
			$('#empty-msg').addClass('d-none');
		}
		else {
			input.classList.add("error");
			var errorCode = iti.getValidationError();
			if(errorCode < 0){
				$('#empty-msg').removeClass('d-none');
			}
			else{
				$('#empty-msg').addClass('d-none');
			}
			errorMsg.innerHTML = errorMap[errorCode];
			errorMsg.classList.remove("d-none");
			phoneVal = false;
		}
	}
});