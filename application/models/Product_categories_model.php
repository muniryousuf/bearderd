<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_categories_model extends My_Model {

    public $fillables = ["name","slug","image","description","parent_id","meta_title","meta_desc","index_follow"];

    public function rules()
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'slug',
                'label' => 'Slug',
                'rules' => 'trim|required|callback_validate_slug',
                'errors' => array(
                    'validate_slug' => 'Slug already taken.'
                )
            )
        );

        return $rules;
    }

    public function category_by_ids($ids, $img_width = 200, $img_height = 300)
    {
        $categories = $this->ProCategories
                            ->find()
                            ->select('id, name, slug, image')
                            ->where_in('id', $ids)
                            ->order_by('field(id, ' . implode(',', $ids) . ')')
                            ->get()
                            ->result_array();

        foreach ($categories as $k => $c)
        {
            $categories[$k]['image'] = $categories[$k]['image'] ? $categories[$k]['image'] : 'no-image.jpg';
            $categories[$k]['image'] = resize_img($categories[$k]['image'], $img_width, $img_height);
        }

        return $categories;
    }

    public function getAllCategories(){
        $categories = $this->ProCategories
            ->find()
            ->select('id, name, slug, image')
            ->get()
            ->result_array();

        return $categories;
    }


    public function getCategoryProduct($name)
    {


        $categories = $this->ProCategories
            ->find()
            ->select('id, name, slug, image')
            ->where('slug', $name)
            ->get()
            ->result_array();



        $productAgainstCat = [];

        foreach ($categories as $value) {
            $productIDs = $this->ProductCategoriesMap->find()->where('category_id', $value['id'])->where('is_feature', 1)
                ->select('product_id')
                ->order_by('id', 'desc')
                ->limit(3)
                ->get()->result_array();

            if (count($productIDs) > 0) {
                $productIDs = $this->return_array($productIDs);
                $productAgainstCat[$value['name']] = $this->Products->products_by_ids($productIDs);
            }
        }
        return $productAgainstCat;
    }

    public function return_array($array){
        $return = array();

        foreach ($array as $v){
            $return[]= $v['product_id'];
        }

        return $return;
    }



    public function getCategoryBaner($array){


        $categories = $this->ProCategories
            ->find()
            ->select('id, name, slug, image')
            ->where_in('slug', $array)
            ->limit(6)
            ->get()
            ->result_array();
        return $categories;
    }


}
