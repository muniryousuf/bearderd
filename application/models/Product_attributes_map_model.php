<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_attributes_map_model extends My_Model {

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;
    const SOFT_DELETED = NULL;

    public $fillables = ["product_id","attributes","stock","price","sale_price","sale_start","sale_end"];

    public function rules()
    {
        $rules = array(
            array(
                'field' => 'product_id',
                'label' => 'Product',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'attributes',
                'label' => 'Attribute',
                'rules' => 'trim|required'
            )
        );

        return $rules;
    }
}
