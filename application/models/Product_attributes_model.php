<?php
;

class Product_attributes_model extends My_Model {

    public $fillables = ["name"];

    public function rules()
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required|callback_validate_name',
                'errors' => array(
                    'validate_name' => 'Attribute with same name already exists.'
                )
            )
        );

        return $rules;
    }
}
