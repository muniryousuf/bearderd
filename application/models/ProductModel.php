<?php

    class ProductModel extends CI_Model{

        public function productList(){
          
            $res = $this->db->select()->order_by('id','desc')->get('products')->result_array();
            return $res;
        }
       
    }

?>
