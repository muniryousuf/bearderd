<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require FCPATH.'vendor/autoload.php';

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\ProductionEnvironment;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;

class Paypal_lib {
	private $environment;
	private $client;
	private $request;
	private $response;
	
	public function __construct($paymentKeys) {
		
		$this->config = array(
			'clientId'     	=> $paymentKeys['clientId'],
            'clientSecret' 	=> $paymentKeys['clientSecret']
		);
		$this->environment = new SandboxEnvironment($this->config['clientId'], $this->config['clientSecret']);
		$this->client = new PayPalHttpClient($this->environment);
	}
	
	public function createOrder($bookingArray = '') {
		$this->request = new OrdersCreateRequest();
		$this->request->prefer('return=representation');
		$this->request->body = array(
			"intent" => "CAPTURE",
			"purchase_units" => [[
				"reference_id" => $bookingArray['reference_id'],
				"amount" => [
					"value" => number_format($bookingArray['totalPrice'], 2),
					"currency_code" => "usd"
				]
			]]
		);
		try { $this->response = $this->client->execute($this->request); }
		catch (HttpException $ex) { return false; }
		return $this->response;
	}
	
	public function captureOrder($order_id) {
		$this->request = new OrdersCaptureRequest($order_id);
		$this->request->prefer('return=representation');
		try { $this->response = $this->client->execute($this->request); }
		catch (HttpException $ex) { return false; }
		return $this->response;
	}

	public function getOrder($order_id) {
		$this->request = new OrdersGetRequest($order_id);
		$this->response = $this->client->execute($this->request);
		try { $this->response = $this->client->execute($this->request); }
		catch (HttpException $ex) { return false; }
		return $this->response;
	}
}