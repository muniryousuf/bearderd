<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require FCPATH.'vendor/autoload.php';

use Twilio\Rest\Client;
class Twilio_lib {
	private $client;
	
	public function __construct($twilio_details) {
		
        $this->config = array(
			'sid'     	=> $twilio_details['sid'],
            'token' 	=> $twilio_details['token'],
            'number' 	=> $twilio_details['number'],
            'message' 	=> $twilio_details['message']
        );
	}
	
	public function sendSMS($to = '') {
        $twilio = new Client($this->config['sid'], $this->config['token']);
        try {
            $message = $twilio->messages->create($to, // to
                                                [
                                                    "body" => $this->config['message'],
                                                    "from" => $this->config['number']
                                                ]
                                            );
            if($message->status == 'sent'){
                return true;
            }
        }
        catch (Twilio\Exceptions\RestException $e) { return false; }
    }
}