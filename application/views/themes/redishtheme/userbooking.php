<?php $theme_view('includes/head'); ?>
<link rel="stylesheet" href="<?php public_assets("plugin/datatable/dataTables.bootstrap4.min.css"); ?>">
<?php $theme_view('includes/headEnd'); ?>
<?php $theme_view('includes/header'); ?>
	
	<div class="mainSection endUser">
		<div class="container">
            <div class="profileSetting selectionBoxMain clearfix">
                <h1 class="profileTitle mb-4"><?php echo esc($title, true); ?></h1>
                <div class="table-responsive">
                    <table id="dtBasicExample" class="table table-striped" width="100%">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Service</th>
                                <th scope="col">Agent Name</th>
                                <th scope="col">Date</th>
                                <th scope="col">Time</th>
                                <th scope="col">Adults</th>
                                <th scope="col">Childrens</th>
                                <th scope="col">Total Bill</th>
                                <th scope="col">Service Status</th>
                                <th scope="col">Payment Status</th>
                                <?php if($stripe['status'] == 1 || $paypal['status'] == 1){ ?><th scope="col">Action</th><?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!$bookings){?>
                                <tr>
                                    <td colspan="11" class="text-center"><h4 class="text-muted">No Booking Found</h4></td>
                                </tr>
                            <?php } else{?>
                            <?php foreach ($bookings as $booking ){ ?>
                                <tr>
                                    <th class="align-middle" scope="row"><?php echo esc($booking['id'], true); ?></th>
                                    <td class="align-middle"><?php echo esc($booking['title'], true); ?></td>
                                    <td class="align-middle"><?php if($booking['agentId'] == 0){ echo 'Any Agent'; }else{ echo esc($booking['agentName'], true); } ?></td>
                                    <td class="align-middle"><?php echo esc($booking['date'], true); ?></td>
                                    <td class="align-middle"><?php echo esc($booking['timing'], true) ?></td>
                                    <td class="align-middle"><?php echo esc($booking['adults'], true) ?></td>
                                    <td class="align-middle"><?php echo esc($booking['childrens'], true) ?></td>
                                    <td class="align-middle"><?php echo '$'.(esc($booking['adults'], true) + esc($booking['childrens'], true))*esc($booking['price'], true) ?></td>
                                    <td class="align-middle"><?php if(esc($booking['serviceStatus'], true) == '' || esc($booking['serviceStatus'], true) == '0'){ echo '<span class="badge badge-warning">Pending</span>'; } else if(esc($booking['serviceStatus'], true) == '1') { echo '<span class="badge badge-success">Confirmed</span>'; } else if(esc($booking['serviceStatus'], true) == '2') { echo '<span class="badge badge-secondary">Cancelled</span>'; } ?></td>
                                    <td class="align-middle text-center"><?php if(!esc($booking['orderId'],true)){ echo '<span class="badge badge-danger">Due</span>'; } else { echo '<span class="badge badge-default d-block text-center">Paid by:</span>'.'<span class="badge badge-secondary d-block text-center">'.$booking['payBy'].'</span>'; } ?></td>
                                    <td class="align-middle">
                                        <?php 
                                            if($stripe['status'] == 1 || $paypal['status'] == 1){
                                                if(!esc($booking['orderId'],true)){ 
                                                    echo '<a href="'.base_url('userbooking/paynow/'.esc($booking['id'], true)).'" class="btn btn-outline-primary btn-sm btn-block">Pay Now</a>'; 
                                                }
                                            } 
                                            if($booking['orderId'] && $booking['receipt_url']) { 
                                                echo '<a target="_blank" href="'.esc($booking['receipt_url'],true).'" class="btn btn-primary btn-sm btn-block">Stripe Invoice</a>';
                                                echo '<a target="_blank" href="invoice/'.esc($booking['orderId'],true).'" class="btn btn-warning btn-sm btn-block">Salon Invoice</a>';  
                                            } 
                                            if ($booking['orderId'] && !$booking['receipt_url']){
                                                echo '<a target="_blank" href="invoice/'.esc($booking['orderId'],true).'" class="btn btn-warning btn-sm btn-block">Salon Invoice</a>'; 
                                            }
                                        ?>
                                    </td>

                                </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>



		</div>
	</div>
	<!-- /mainSection -->
	
<?php $theme_view('includes/foot'); ?>
<script src="<?php public_assets("plugin/datatable/jquery.dataTables.min.js"); ?>"></script>
<script src="<?php public_assets("plugin/datatable/dataTables.bootstrap4.min.js"); ?>"></script>
<script>
    $(document).ready(function () {
        $('#dtBasicExample').DataTable({
            "pagingType": "numbers"
        });
    });
</script>
<?php $theme_view('includes/footEnd'); ?>