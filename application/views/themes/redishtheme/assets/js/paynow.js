(function($){
	'use strict';
		
		//services show in div's
		let $selectMethod 		= $('#selectMethod');//
		var form 				= $('#serviceUserForm');//
		var is_card 			= false;
		var is_paypal 			= false;
		var card 				= '';

		$selectMethod.on('change', function(){
			$(this).find("option:selected").each(function(){
				var optionValue = $(this).attr("value");
				if(optionValue == 0){//<option> by cash
					is_card 	= false;
					is_paypal 	= false;
					$("#payment-card").addClass('d-none');
					$('#payment-paypal').addClass('d-none');
					$('#serviceSubmit').removeClass('d-none');
				}
				if(optionValue == 1){//<option> stripe
					is_card 	= true;
					is_paypal 	= false;
					$('#payment-card').removeClass('d-none');
					$('#serviceSubmit').removeClass('d-none');
					$('#payment-paypal').addClass('d-none');
				}
				if(optionValue == 2){// <option> paypal
					is_card 	= false;
					is_paypal 	= true;
					$("#payment-card").addClass('d-none');
					$("#serviceSubmit").addClass('d-none');
					$('#payment-paypal').removeClass('d-none');
				}
			});
		});
		if(stripePub && stripeStatus == true){
			// Card
			var elements = stripe.elements();

			var style = {
				base: {
					color: '#32325d',
					fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
					fontSmoothing: 'antialiased',
					fontSize: '16px',
					'::placeholder': {
					color: '#aab7c4'
					}
				},
				invalid: {
					color: '#fa755a',
					iconColor: '#fa755a'
				}
			};

			// Create an instance of the card Element.
			var card = elements.create('card', {style: style});

			// Add an instance of the card Element into the `card-element` <div>.
			card.mount('#card-element');

			// Handle real-time validation errors from the card Element.
			card.on('change', function(event) {
			var displayError = document.getElementById('card-errors');
			if (event.error) {
				displayError.textContent = event.error.message;
			} else {
				displayError.textContent = '';
			}
			});
		}

		// Handle form submission.
		form.on('submit', function(event) {
			event.preventDefault();
			if(is_card) { // When Stripe Select
				stripe.createToken(card).then(function(result) {
					if (result.error) {
					// Inform the user if there was an error.
					var errorElement = document.getElementById('card-errors');
					errorElement.textContent = result.error.message;
					} else {
						submit_form(result);
					}
				});
			} else { // When By Cash Select
				window.location.replace(base + "userbooking/");
			}
		});

		function report_errors(data) {
			$.each(data, function(key, value) {
				if(value!=''){
					$('.stage').addClass('d-none');
					$('.loaderBeforeSubmit').removeClass('d-none');
				}
				else{
					$('.stage').addClass('d-none');
					$('.loaderBeforeSubmit').removeClass('d-none');
				}

				if(data.serviceAdded == false){
					$('.stage').addClass('d-none');
					$('.loaderBeforeSubmit').removeClass('d-none');
				}
			});
		}

		// Function by Submit Form
		function submit_form(result = null, callback = false) {
			if(is_card){
				form.append('<input type="hidden" name="stripeToken" value="' + result.token.id + '">');
			} else if(is_paypal) {
				form.append('<input type="hidden" name="paypal" value="true" />');
			}

			$('.stage').removeClass('d-none');
			$('.loaderBeforeSubmit').addClass('d-none');
			$.ajax({
				type: "POST",
				url: form.attr('action'),
				data : form.serialize(),
				dataType: "json",

				success: function(data) {
					if(!callback) {
						report_errors(data);
						if(data.serviceAdded == true){
							if(data.payment.orderid == ''){
								window.location.replace(base + "userbooking/")
							}
							else{
								window.location.replace(base + "invoice/" + data.payment.orderid);
							}
						}
					} else callback(data);
				}
			});
		}
		if(ppClientId && ppStatus == true){
			var currentBooking = null;
			paypal.Buttons({
				funding: 'disallowed',
				createOrder: function() {
					let promise = new Promise(function(resolve, reject) {
						submit_form(null, function(data) {
							if(data.serviceAdded) {
								currentBooking = data.payment.bookingId;
								resolve(data.payment.orderid);
							} else { 
								report_errors(data);
								reject('Invalid Form Data.');
							}
						});
					}).then(function(orderid) { return orderid; })
					.catch(e => console.error(e));

					return promise;
				},
				onApprove: function(data) {
					const errorReportPayPal = $('#paypal-errors');
					$('#fscreenLoader').removeClass('d-none');
					$.ajax({
						url: base+`homepage/confirmation/${data.payerID}/${data.orderID}`,
						success: function(data) {
							if (data != 'Access Denied.') {
								data = JSON.parse(data);
								if (data.type == 'success') {
									window.location.replace(base + "invoice/" + data.orderid);
								}
							} else {
								errorReportPayPal.html('Your payment can\'t be processed at this moment.');
							}
						}
					});
				},
				onCancel: function (data) {
					const errorReportPayPal = $('#paypal-errors');
					$('#fscreenLoader').removeClass('d-none');
					$.ajax({
						url: base+`homepage/cancelation/${data.orderID}`,
						success: function(data) {
							console.log(data);
							if (data.type != 'error') {
								data = JSON.parse(data);
								if (data.type == 'success' && data.orderid == '') {
									location.reload();
								}
							} else {
								errorReportPayPal.html('Something went wrong please try again.');
							}
						}
					});
				}
			}).render('#payWithPaypal');
		}
})(jQuery);