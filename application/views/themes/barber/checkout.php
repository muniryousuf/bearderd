<?php $theme_view('includes/head'); ?>
<link rel="stylesheet" href="<?php $assets('plugins/countries/build/css/intlTelInput.css'); ?>">
<?php $theme_view('includes/headEnd'); ?>
<?php $theme_view('includes/header'); ?>



<div id="main" >
    <div class="container main-title-section mainSection ">

	    <form id="frm-checkout" action="<?php anchor_to('/process-order')?>" method="post">
	        <div class="row">
	        	<div class="col-md-8">
	    			<div class="row">
	    				<div class="col-12 col-md-6 form-group">
	    					<label for="billing_first_name">First Name <span class="required">*</span></label>
	    					<input type="text" id="billing_first_name" name="billing[first_name]" class="form-control" value="" maxlength="50" required />
	    				</div>
	    				<div class="col-12 col-md-6 form-group">
	    					<label for="billing_last_name">
                                Last name <span class="required">*</span></label>
	    					<input type="text" id="billing_last_name" name="billing[last_name]" class="form-control" value="" maxlength="50" required />
	    				</div>
	    			</div>

	    			<div class="form-group">
    					<label for="billing_country">Country <span class="required">*</span></label>
    					<input type="text" id="billing_country" class="form-control" value="UK" readonly />
    				</div>

    				<div class="form-group">
    					<label for="billing_address1">
                            Street and house number <span class="required">*</span></label>
    					<input type="text" id="billing_address1" name="billing[address1]" class="form-control" placeholder="Street name and house number" value="" maxlength="255" required />
    					<input type="text" id="billing_address2" name="billing[address2]" class="form-control mt-2" placeholder="Appartment, suite, unit etc. (optional)" maxlength="255" value="" />
    				</div>

    				<div class="form-group">
    					<label for="billing_postalcode">Postcode <span class="required">*</span></label>
    					<input type="text" id="billing_postalcode" name="billing[postalcode]" class="form-control" value="" maxlength="10" required />
    				</div>

    				<div class="form-group">
    					<label for="billing_city">
                            Place <span class="required">*</span></label>
    					<input type="text" id="billing_city" name="billing[city]" class="form-control" value="" maxlength="50" required />
    				</div>

    				<div class="form-group">
    					<label for="phone">
                            Telephone (optional)</label>
    					<input type="text" id="phone" name="phone" class="form-control" maxlength="50" value="" />
    				</div>

    				<div class="form-group">
    					<label for="email">
                            E-mail address<span class="required">*</span></label>
    					<input type="email" id="email" name="email" class="form-control" value="" maxlength="50" required />
    				</div>

    				<div class="form-group">
    					<label for="comments">Order notes (optional)</label>
    					<textarea id="comments" name="comments" class="form-control" placeholder="Notes about your order, for example special notes for delivery."></textarea>
    				</div>


                    <label for="">Select Payment Method</label>
                    <div class="singleInputform form-group">
                        <select class="custom-select" name="selectPayment" id="selectMethod" required>
                            <option value="0" selected>By Cash</option>
                            <?php if($stripe['status'] == 1 && $stripe['stripe_publishable_key'] && $stripe['stripe_api_key']){ ?><option value="1">Credit Card</option><?php } ?>
                        </select>
                    </div>
                    <div id="payment-card" data-val="1" class="d-none">
                        <div id="card-element"><!-- A Stripe Element will be inserted here. --></div>
                        <!-- Used to display form errors. -->
                        <small id="card-errors" role="alert" class="form-text text-danger"></small>
                    </div>

	        	</div>

	        	<div class="col-md-4">
	        		<div class="cart-total">
						<h2> Your order </h2>
						<table class="table">
							<tbody>
                            <?php foreach ($cartData['items'] as $value) { ?>
								<tr class="cart-product">
									<td>
                                        <?php echo  $value['product_name'] ?>

										x <?php echo  $value['quantity'] ?>
									</td>
									<th>

                                       <?php echo  "£". $value['price'] * $value['quantity'] ;
                                                $subTotal+=   $value['price'] * $value['quantity'];

                                       ?>;
                               	</th>
								</tr>
								<?php } ?>

								<tr class="cart-subtotal">
									<th>Subtotal</th>
									<td> <?php echo "£". $subTotal;?></td>
								</tr>
								<tr class="cart-shipping">
									<th>
                                        shipment</th>
									<td>
                                        Free Shipping</td>
								</tr>

								<tr class="order-total">
									<th>Total</th>
									<td>
										<strong><?php echo "£". $subTotal;?></strong>

									</td>
								</tr>
							</tbody>
						</table>

                        <input type="hidden" name="payment_method" value="cash">
						<p>
							<button type="submit" class="place-order d-block theme_btn text-uppercase text-center">
                                Place an order</button>
						</p>
                    				</div>
	        	</div>
	        </div>
	    </form>
    </div>
</div>
<?php $theme_view('includes/footer'); ?>
<?php $theme_view('includes/foot'); ?>
<script src="<?php $assets("plugins/moment/moment.min.js"); ?>"></script>
<script src="<?php $assets("plugins/datepicker/bootstrap-datetimepicker.min.js"); ?>"></script>
<script src="<?php $assets("js/magnific.popup.min.js"); ?>"></script>
<?php if($stripe['status'] == 1 && $stripe['stripe_publishable_key'] && $stripe['stripe_api_key']){ ?>
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        // Stripe Publishable Key.
        var stripe = Stripe('<?php echo $stripe['stripe_publishable_key']; ?>');

        var is_card 			= false;
        var card 				= '';

        $("#selectMethod").on('change', function(){

            $(this).find("option:selected").each(function(){
                var optionValue = $(this).attr("value");
                if(optionValue == 0){//<option> by cash
                    is_card 	= false;
                    $("#payment-card").addClass('d-none');
                    $('#payment-paypal').addClass('d-none');
                    $('#serviceSubmit').removeClass('d-none');
                }
                if(optionValue == 1){//<option> stripe
                    is_card 	= true;
                    $('#payment-card').removeClass('d-none');
                    $('#serviceSubmit').removeClass('d-none');
                    $('#payment-paypal').addClass('d-none');
                }
            });
        });

        // Card
        var elements = stripe.elements();

        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');
        // Handle real-time validation errors from the card Element.
        card.on('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        // Handle form submission.
        var form = $('#frm-checkout');
        form.on('submit', function(event) {
            event.preventDefault();

            if(is_card) { // When Stripe Select
                stripe.createToken(card).then(function(result) {
                    if (result.error) {
                        // Inform the user if there was an error.
                        var errorElement = document.getElementById('card-errors');
                        errorElement.textContent = result.error.message;
                    } else {
                        form.append('<input type="hidden" name="stripeToken" value="' + result.token.id + '">');

                        $.ajax({
                            type: "POST",
                            url: form.attr('action'),
                            data : form.serialize(),
                            dataType: "json",

                            success: function(data) {

                                console.log(data)



                                if(!callback) {
                                    report_errors(data);
                                    if(data.serviceAdded == true){
                                        if(data.payment.orderid == ''){
                                            location.reload();
                                        }
                                        else{
                                            window.location.replace(base + "invoice/" + data.payment.orderid);
                                        }
                                    }
                                } else callback(data);
                            }
                        });
                    }
                });
            } else {
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data : form.serialize(),
                    dataType: "json",

                    success: function(data) {

                        if (data.success == true) {
                            window.location.replace(<?php echo base_url('/');?>);
                        }
                    }
                });
            }
        });


    </script>

<?php } ?>

<?php $theme_view('includes/footEnd'); ?>

