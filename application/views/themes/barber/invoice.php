<?php $theme_view('includes/head'); ?>
<?php $theme_view('includes/headEnd'); ?>
<?php $theme_view('includes/header'); ?>
	
	<div class="breadcumb-area jarallax"></div>
    <section class="blog-details-area section">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 offset-lg-2">
					<?php 
						if($invMsg = $this->session->flashdata('invMsg')){
							$inv_class = $this->session->flashdata('inv_class');
					?>
							<div class="alert <?php echo esc($inv_class, true);?>"><?php echo esc($invMsg, true);?></div>
					<?php
						}
					?>
					<div class="selectionBoxMain signupSec">
						<div id="printableArea">
							<div class="section-heading-three">
								<h2>Paid Successfully</h2>
								<p>Thank you for your service.</p>
							</div>
							
							<div class="row">
								<div class="col-md-5">
									<img class="img-fluid img-thumbnail" alt="Invoce Template" src="<?php uploads("img/".$service['image']);?>" />
								</div>
								<div class="col-md-7 text-xs-right">
									<h4><?php echo esc($service['title'], true) ?></h4>
									<p class="mb-2"><i class="icon-mobile2"></i> <?php echo esc($user['phone'], true) ?><br><i class="icon-envelop"></i>  <?php echo esc($user['email'], true) ?></p>
									<?php if($order['transectionId']){ ?>
										<h4>Transection Id</h4>
										<p class="mb-0"><?php echo esc($order['transectionId'], true) ?></p>
									<?php } else {?>
										<h4>Order Id</h4>
										<p class="mb-0"><?php echo esc($order['orderId'], true) ?></p>
									<?php }?>
								</div>
							</div>
							<br />
							<div class="table-responsive">
								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>Description</th>
											<th>Amount</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="col-md-8">Your appointment of <strong>"<?php echo esc($service['title'], true) ?>"</strong> have been booked for <strong>"<?php echo esc($booking['date'], true) ?>"</strong> & <strong>"<?php echo esc($booking['timing'], true) ?>"</strong>.</td>
											<td class="col-md-4"><?php echo esc($order['paid_amount'], true) ?> <?php echo esc($order['paid_currency'], true) ?></td>
										</tr>
										
										<tr>
											<td class="align-middle text-right"><strong>Paid By: </strong></td>
											<td class="align-middle"><strong class="text-primary"><?php echo esc($booking['payBy'], true) ?></strong></td>
										</tr>
										
										<tr>
											<td class="align-middle text-right"><strong>Total Amount: </strong></td>
											<td class="align-middle"><strong><?php echo esc($order['paid_amount'], true) ?> <?php echo esc($order['paid_currency'], true) ?></strong></td>
										</tr>
										<?php if($order['receipt_url']){ ?>
											<tr>
												<td class="align-middle text-right"><strong>Check your Stripe Invoice: </strong></td>
												<td class="align-middle"><a href="<?php echo esc($order['receipt_url'], true) ?>" target="_blank" class="btn btn-primary btn-sm">Stripe Invoice</a></td>
											</tr>
										<?php }?>
										<tr>
											<td class="text-right align-middle"><h5 class="mb-0"><strong>Total:</strong></h5></td>
											<td class="text-left align-middle"><h5 class="mb-0"><strong><?php echo esc($order['paid_amount'], true) ?> <?php echo esc($order['paid_currency'], true) ?></strong></h5></td>
										</tr>
										<tr>
											<td class="text-right align-middle"><h5 class="mb-0"><strong>Date:</strong></h5></td>
											<td class="text-left align-middle"><p class="mb-0"><?php echo esc($order['created'], true) ?></p></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<button type="button" class="mb-3 btn3 btn-block" onclick="printDiv('printableArea')"><span>Print Invoice</span></button>

						<?php if(!$this->session->userdata('id')){ ?>
							<div class="signUpWithSocial">
								<h3>Login / Signup for keep tracking your booking</h3>
							</div>
							<div class="loginGoSignup pt-0"><a class="bg-default" href="<?php echo base_url('login') ?>">Login</a> / <a href="<?php echo base_url('login/signUp') ?>">Sign Up</a></div>
						<?php } else{ ?>
							<div class="text-center"><a class="btn1" href="<?php echo base_url('userbooking') ?>"><span>Go to All Bookings</span></a></div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- /section -->

<?php $theme_view('includes/footer'); ?>
<?php $theme_view('includes/foot'); ?>
<script type="text/javascript">
	function printDiv(divName) {
		var printContents = document.getElementById(divName).innerHTML;
		var originalContents = document.body.innerHTML;
		document.body.innerHTML = printContents;
		window.print();
		document.body.innerHTML = originalContents;
	}
</script>
<?php $theme_view('includes/footEnd'); ?>