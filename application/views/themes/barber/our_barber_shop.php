	<?php $theme_view('includes/head'); ?>
<?php $theme_view('includes/headEnd'); ?>
<?php $theme_view('includes/header'); ?>
	<div class="main-title-section mainSection franchise" style="background: url('https://i.ibb.co/YdrB78G/DSC3747.jpg'); background-size: cover; background-position: center;">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-6 m-lg-auto align-center text-center">
						<div class="img-box">
							<img src="http://beardedbarberclub.softdemo.co.uk/application/views/themes/barber/assets/images/logo.png">
						</div>
						<h3>BEARDED BARBER<br>CLUB</h3>

						<ul>
							<li>
								<a href="#" class="btn btn1">
									<span>Facebook</span></a>
							</li>
							<li>
								<a href="#" class="btn formSubmitBtn btn2">
									<span>Instagram</span></a>
							</li>
							<li>
								<a href="#" class="btn btn1">
									<span>Book An Appointment</span></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="overlay"></div>
	</div>
<!--	<div class="franchise-sec1">-->
<!--		<div class="container">-->
<!--			<div class="row">-->
<!--				<div class="col-12 col-lg-8 text-center m-lg-auto">-->
<!--					<h3>Welcome to Bearded Barber Club</h3>-->
<!--					<p>Bearded Barber Club established in 2020, taking barbering to the next level. We have bought back the elements of traditional barbering where our no1 priority is the quality of our haircuts rather then the focusing on quantity. We operate our shop in a friendly relaxed environment where we offer a range of refreshing beverage including premium hot drinks. -->
<!--Located in the heart of Birmingham city centre a few minutes’ walk from Broad street.  We offer both a walk-in and appointment services. Our team of barbers have a vast amount of knowledge in the barbering industry. We offer a range of different services, including hot towel shaves, facial waxing and more. Your hair/beard will be good hand when you visit us.-->
<!--					</p>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
	<div class="franchise-gal">
		<div class="row p-0">
			<div class="col-6 p-0">
				<img src="<?php $assets ('images/gallery/image-1.png'); ?>">
			</div>
			<div class="col-6 p-0">
				<img src="<?php $assets ('images/gallery/img2.png'); ?>">
			</div>
		</div>
		<div class="row p-0">
			<div class="col-3 p-0">
				<img src="<?php $assets ('images/gallery/img7.png'); ?>">
			</div>
			<div class="col-3 p-0">
				<img src="<?php $assets ('images/gallery/img8.png'); ?>">
			</div>
			<div class="col-3 p-0">
				<img src="<?php $assets ('images/gallery/image-1.png'); ?>">
			</div>
			<div class="col-3 p-0">
				<img src="<?php $assets ('images/gallery/img10.png'); ?>">
			</div>
		</div>
		<div class="row p-0">
			<div class="col-6 p-0">
				<img src="<?php $assets ('images/gallery/img3.png'); ?>">
			</div>
			<div class="col-6 p-0">
				<img src="<?php $assets ('images/gallery/img4.png'); ?>">
			</div>
		</div>
		<div class="row p-0">
			<div class="col-6 p-0">
				<img src="<?php $assets ('images/gallery/img5.png'); ?>">
			</div>
			<div class="col-6 p-0">
				<img src="<?php $assets ('images/gallery/img6.png'); ?>">
			</div>
		</div>
	</div>
	<div class="main-title-section mainSection frn-s3" style="background: url('https://i.ibb.co/WG1PK8f/1.png'); background-size: cover; background-position: center;">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-6 m-lg-auto align-center text-center">
						<p>Bearded Barber Club established in 2020, taking barbering to the next level. We have bought back the elements of traditional barbering where our no1 priority is the quality of our haircuts rather then the focusing on quantity. We operate our shop in a friendly relaxed environment where we offer a range of refreshing beverage including premium hot drinks.
Located in the heart of Birmingham city centre a few minutes’ walk from Broad street.  We offer both a walk-in and appointment services. Our team of barbers have a vast amount of knowledge in the barbering industry. We offer a range of different services, including hot towel shaves, facial waxing and more. Your hair/beard will be good hand when you visit us..</p>
					</div>
				</div>
			</div>
			<div class="overlay"></div>
	</div>

	<!-- /mainSection -->
	<?php $theme_view('includes/footer'); ?>
<?php $theme_view('includes/foot'); ?>

<?php $theme_view('includes/footEnd'); ?>
