<?php $theme_view('includes/head'); ?>
<?php $theme_view('includes/headEnd'); ?>
<?php $theme_view('includes/header'); ?>
<?php $quantity = 1; ?>
<style>
    header {
        position: relative;
        top: 0;
    }

    .product-slider {
        padding: 45px;
    }

    .product-slider #carousel {
        border: 4px solid #1089c0;
        margin: 0;
    }

    .product-slider #thumbcarousel {
        margin: 12px 0 0;
        padding: 0 45px;
    }

    .product-slider #thumbcarousel .item {
        text-align: center;
    }

    .product-slider #thumbcarousel .item .thumb {
        border: 4px solid #cecece;
        width: 25%;
        margin: 0 2px;
        display: inline-block;
        vertical-align: middle;
        cursor: pointer;
        max-width: 98px;
    }

    .product-slider #thumbcarousel .item .thumb:hover {
        border-color: #1089c0;
    }

    .product-slider .item img {
        width: 100%;
        height: auto;
    }

    .carousel-control {
        color: #0284b8;
        text-align: center;
        text-shadow: none;
        font-size: 30px;
        width: 30px;
        height: 30px;
        line-height: 20px;
        top: 23%;
    }

    .carousel-control:hover, .carousel-control:focus, .carousel-control:active {
        color: #333;
    }

    .carousel-caption, .carousel-control .fa {
        font: normal normal normal 30px/26px FontAwesome;
    }

    .carousel-control {
        background-color: rgba(0, 0, 0, 0);
        bottom: auto;
        font-size: 20px;
        left: 0;
        position: absolute;
        top: 30%;
        width: auto;
    }

    .carousel-control.right, .carousel-control.left {
        background-color: rgba(0, 0, 0, 0);
        background-image: none;
    }

    img.thumb {
        padding: 0 1px;
    }

    .owl-buttons {
        /* display: none; */
    }

    .owl-carousel:hover .owl-buttons {
        display: block;
    }

    .owl-item {
        text-align: center;
    }

    .owl-theme .owl-controls .owl-buttons div {
        background: transparent;
        color: #869791;
        font-size: 40px;
        line-height: 0px;
        margin: 0;
        padding: 0 60px;
        position: absolute;
        top: 0;
        display: flex;
        align-items: center;
        height: 100%;
    }

    .owl-theme .owl-controls .owl-buttons .owl-prev {
        left: 0;
        padding-left: 10px;
    }

    .owl-theme .owl-controls .owl-buttons .owl-next {
        right: 0;
        padding-right: 10px;
    }
</style>

<section id="product" class="product-detail">
    <div class="container">
        <div class="row pt-3 pb-5">
            <div class="col-md-5 ">
                <div id="sync1" class="slider owl-carousel">

                    <?php foreach ($images as $k => $img) { ?>
                        <div class="item">
                            <img src="<?php $assets($img['image']); ?>">
                        </div>
                       <!-- <li data-target="#procuctCarousel" data-slide-to="<?/*= $k + 1 */?>">
                            <img src="<?/*= $img['image'] */?>" class="img-fluid"/>
                        </li>-->
                    <?php } ?>
                </div>
                <div id="sync2" class="mt-1 navigation-thumbs owl-carousel">
                    <?php foreach ($images as $k => $img) { ?>
                        <div class="item">
                            <img class="thumb" src="<?php $assets($img['image']); ?>">
                        </div>
                    <?php } ?>
                </div>

            </div>
            <div class="col-md-7">
                <div class="product-title"><?= $productData['name'] ?></div>
                <div class="product-desc"><?= $productData['short_description'] ?>
                </div>
                <div class="product-rating"><i class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i
                            class="fa fa-star gold"></i> <i class="fa fa-star gold"></i> <i class="fa fa-star-o"></i>
                </div>
                <hr>
                <div class="product-price">£ <?= $productData['price'] ?></div>
                <div class="product-stock">In Stock</div>
                <hr>

                <form id="frm-addtocart" action="<?= base_url('/cart/add') ?>" method="POST" class="d-none d-md-block">

                    <input type="hidden" name="action" value="addtocart" />
                    <input type="hidden" name="product" value="<?= $productData['id']; ?>" />
                    <input type="hidden" name="quantity" value="<?= $quantity ?>" min="1">
                    <input type="hidden" name="price" value="<?= $productData['price']; ?>" />

                    <div class="btn-group cart">
                        <button type="submit" class="btn btn-success">
                            Add to cart
                        </button>
                    </div>

                    <!--<div class="btn-group wishlist">
                        <button type="button" class="btn btn-danger">
                            Add to wishlist
                        </button>
                    </div>-->

                </form>

            </div>

            <?php /** ?>
             * <div class="col-md-7">
             * <div class="pro-d">
             * <p id="price" class="border_bottom price">
             *
             * <span class="text_black"><?= $product['price'] ?></span>
             *
             * </p>
             * <form id="frm-addtocart" action="<?= url('/cart/add') ?>" method="POST"
             * class="d-none d-md-block frm-check"
             *
             * <div class="short_description mb-4">
             * Thisi i
             * </div>
             *
             *
             * <input type="hidden" name="action" value="addtocart"/>
             * <input type="hidden" name="product" value="<?= $product['id'] ?>"/>
             * <div class="proAttrDiv">
             *
             * <?php if (isset($attribute_vals) && count($attribute_vals) > 0) {
             * if ($attribute_vals) {
             * for ($a = 0; $a < $quantity; $a++) {
             * $attrCount = 1;
             *
             * ?>
             *
             * <div class="form-row proAttrRow">
             *
             * <?php foreach ($attribute_vals as $k1 => $v1) { ?>
             * <div class="form-group col-md-6">
             * <label for="inputState"><strong><?= $v1['name'] . " " . ($a + 1) ?></strong></label>
             * <select id="attr<?= $k1 ?>" name="attribute[<?= $a ?>][<?= $k1 ?>]"
             * class="form-control form_elements product-attribute"
             * data-key="<?= $k1 ?>" required>
             * <option value="">
             * Choose an option
             * </option>
             * <?php foreach ($v1['childs'] as $k2 => $v2) { ?>
             * <option value="<?= $k2 ?>"><?= $v2 ?></option>
             * <?php } ?>
             * </select>
             * </div>
             * <?php
             * if ($attrCount == 3) {
             * $attrCount = 1;
             * echo '</div><div class="form-row proAttrRow">';
             * } else {
             * $attrCount++;
             * }
             * }
             * ?>
             * </div>
             * <?php
             * }
             * }
             * }
             * ?>
             * </div>
             *
             * <div id="cart-btn-div" class="form-row product-quantity">
             * <div class="counter col-md-3">
             * <div class="px-0 b-l quant_btn">
             * <button type="button" class="btn btn-minus form_elements text-center">-</button>
             * </div>
             * <div class="form-group m-0">
             * <label for="inputQuantity" class="sr-only">Quantity</label>
             * <input type="text" name="quantity" value="<?= $quantity ?>" min="1"
             * class="form-control form_elements text-center numberonly quantity">
             * </div>
             * <div class="px-0 b-r quant_btn">
             * <button type="button" class="btn btn-plus form_elements text-center">+</button>
             * </div>
             * </div>
             * <div class="col-md-9">
             * <button type="button" id="btn-addtocart" class="theme_btn custom-btn2">Order now!</button>
             * </div>
             * </div>
             *
             * <div id="stock-alert-div" style="display: none">
             * <div class="alert alert-danger stock-alert">Product out of stock</div>
             *
             * <div class="card text-center" style="max-width: 25rem;">
             * <div class="card-header">
             * Email me when stock is available
             * </div>
             * <div class="card-body">
             * <input id="restock_email" type="email" class="form-control" name="restock_email"
             * placeholder="E-mailadres"/>
             * <button id="restock_btn" type="button" class="theme_btn mt-3">Subscribe</button>
             * </div>
             * </div>
             * </div>
             * </form>
             *
             * <div class="product_meta text_gray mt-3">
             * <span class="posted_in">Categories
             * <?php foreach ($categories as $cat) { ?>
             * <a class="text_gray text_deco_hover"
             * href="<?= url('/product-category/' . $cat['slug']) ?>"><?= $cat['name'] ?></a>
             * &nbsp;
             * <?php } ?>
             * </span>
             *
             * <span class="sku_wrapper">SKU:
             * <span class="sku"><?= $product['sku'] ? $product['sku'] : 'N/A' ?></span>
             * </span>
             *
             * <div class="tagged_as mt-3">Tags:
             * <?php foreach (explode(',', $product['tags']) as $tag) { ?>
             * <a class="text_gray"
             * href="<?= url('/product-tag/' . str_replace(' ', '-', $tag)) ?>"><?= $tag ?></a>
             * <?php } ?>
             * </div>
             * </div>
             *
             * <div class="share_icon pt-5">
             * <p class="mb-2"><strong class="text_black">Share this product</strong></p>
             * <a href="http://www.facebook.com/sharer.php?u=<?= urlencode(url('/product/' . $product['slug']))  ?>"
             * class="btn btn-fb"><i class="fab fa-facebook-f"></i></a>
             * </div>
             * </div>
             * </div>
             * <?php **/ ?>
        </div>
    </div>
</section>

<?php $theme_view('includes/footer'); ?>
<?php $theme_view('includes/foot'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        var sync1 = $("#sync1");
        var sync2 = $("#sync2");

        sync1.owlCarousel({
            singleItem: true,
            autoPlay: true,
            slideSpeed: 1000,
            navigation: false,
            pagination: false,
            afterAction: syncPosition,
            responsiveRefreshRate: 200,
            navigation: true,
            navigationText: ['<span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-chevron-circle-left fa-stack-1x fa-inverse"></i></span>', '<span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-chevron-circle-right fa-stack-1x fa-inverse"></i></span>'],
        });

        sync2.owlCarousel({
            items: 5,
            itemsDesktop: [1199, 10],
            itemsDesktopSmall: [979, 10],
            itemsTablet: [768, 8],
            itemsMobile: [479, 4],
            pagination: false,
            responsiveRefreshRate: 100,
            navigation: true,
            navigationText: ['<span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-chevron-circle-left fa-stack-1x fa-inverse"></i></span>', '<span class="fa-stack"><i class="fa fa-circle fa-stack-1x"></i><i class="fa fa-chevron-circle-right fa-stack-1x fa-inverse"></i></span>'],
            afterInit: function (el) {
                el.find(".owl-item").eq(0).addClass("synced");
            }
        });

        function syncPosition(el) {
            var current = this.currentItem;
            $("#sync2")
                .find(".owl-item")
                .removeClass("synced")
                .eq(current)
                .addClass("synced")
            if ($("#sync2").data("owlCarousel") !== undefined) {
                center(current)
            }
        }

        $("#sync2").on("click", ".owl-item", function (e) {
            e.preventDefault();
            var number = $(this).data("owlItem");
            sync1.trigger("owl.goTo", number);
        });

        function center(number) {
            var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
            var num = number;
            var found = false;
            for (var i in sync2visible) {
                if (num === sync2visible[i]) {
                    var found = true;
                }
            }

            if (found === false) {
                if (num > sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                } else {
                    if (num - 1 === -1) {
                        num = 0;
                    }
                    sync2.trigger("owl.goTo", num);
                }
            } else if (num === sync2visible[sync2visible.length - 1]) {
                sync2.trigger("owl.goTo", sync2visible[1])
            } else if (num === sync2visible[0]) {
                sync2.trigger("owl.goTo", num - 1)
            }

        }

    });
</script>
<?php $theme_view('includes/footEnd'); ?>
<?php /** ?>
 * <section id="mobile-btn-div" class="d-block d-md-none">
 * <div class="container">
 * <a id="mobile-order-btn" href="javascript:void(0)" class="d-block theme_btn text-center">Order now!</a>
 * </div>
 * </section>
 *
 *
 *
 * <aside id="mobile-order-div" class="d-none">
 * <div class="sidebar_close_btn">
 * <span></span>
 * </div>
 * <div class="frm"></div>
 * </aside>
 *
 * <section id="product_description">
 * <div class="container">
 * <div class="row">
 * <div class="col-12">
 * <nav>
 * <div class="nav flex-column flex-sm-row" id="nav-tab" role="tablist">
 * <a class="nav-item text_gray active" id="nav-beschrijving-tab" data-toggle="tab"
 * href="#nav-beschrijving" role="tab" aria-controls="nav-beschrijving" aria-selected="true">DESCRIPTION</a>
 * <a class="nav-item text_gray" id="nav-extra-informatie-tab" data-toggle="tab"
 * href="#nav-extra-informatie" role="tab" aria-controls="nav-extra-informatie"
 * aria-selected="false">
 * EXTRA INFORMATION</a>
 * </div>
 * </nav>
 * <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
 * <div class="tab-pane fade show active" id="nav-beschrijving" role="tabpanel"
 * aria-labelledby="nav-beschrijving-tab">
 * <?= prepare_content($product['description']) ?>
 * </div>
 * <div class="tab-pane fade" id="nav-extra-informatie" role="tabpanel"
 * aria-labelledby="nav-extra-informatie-tab">
 *
 * <?php
 *
 * if (isset($attribute_vals) && count($attribute_vals) > 0) {
 *
 * foreach ($attribute_vals as $av) { ?>
 * <div class="row info_box px-2 py-4">
 * <strong class="col-lg-2 pl-lg-4"><?= $av['name'] ?></strong>
 * <span class="col-lg-9"><?= implode(', ', $av['childs']) ?></span>
 * </div>
 * <?php }
 * } ?>
 * </div>
 * </div>
 * </div>
 * </div>
 * </div>
 * </section>
 *
 * <?php if ($upsell_products) { ?>
 * <section id="product_related">
 * <div class="container">
 * <div class="row pb-5">
 * <div class="col-12 divider pb-5">
 * <hr>
 * </div>
 * <div class="col-12">
 * <h4 class="border_bottom">
 * YOU MAY ALSO LIKE THIS ...</h4>
 * </div>
 * <?php foreach ($upsell_products as $pro) { ?>
 * <div class="col-lg-4 pb-4">
 * <div class="row px-3 related_product_inner">
 * <div class="col-3 px-0 img_wrapper">
 * <?php if ($pro['sale_price']) { ?>
 * <span class="percent bg_pink text-white d-flex align-items-center justify-content-center">%</span>
 * <?php } ?>
 * <a href="<?= url('/product/' . $pro['slug']) ?>">
 * <img src="<?= $pro['image'] ?>" class="img-fluid d-block mx-auto"/>
 * </a>
 * </div>
 * <div class="col-9 pr-0 related_product_description">
 * <h5><a href="<?= url('/product/' . $pro['slug']) ?>"
 * class="text_black"><?= $pro['name'] ?></a></h5>
 * <p>
 * <?php if ($pro['sale_price']) { ?>
 * <del class="text_gray"><?= format_money($pro['price']) ?></del>
 * <span class="text_black"><?= format_money($pro['sale_price']) ?></span>
 * <?php } else { ?>
 * <span class="text_black"><?= format_money($pro['price']) ?></span>
 * <?php } ?>
 * </p>
 * <div class="btn_wrapper">
 * <a href="<?= url('/product/' . $pro['slug']) ?>" class="theme_btn text-white">
 * BUY NOW</a>
 * </div>
 * </div>
 * </div>
 * </div>
 * <?php } ?>
 * </div>
 * </div>
 * </section>
 * <?php } ?>
 *
 * <?php if ($related_products) { ?>
 * <section id="product_related">
 * <div class="container">
 * <div class="row pb-5">
 * <div class="col-12 divider pb-5">
 * <hr>
 * </div>
 * <div class="col-12">
 * <h4 class="border_bottom">
 * RELATED PRODUCTS</h4>
 * </div>
 * <?php foreach ($related_products as $pro) { ?>
 * <div class="col-lg-4 pb-4">
 * <div class="row px-3 related_product_inner">
 * <div class="col-3 px-0 img_wrapper">
 * <?php if ($pro['sale_price']) { ?>
 * <span class="percent bg_pink text-white d-flex align-items-center justify-content-center">%</span>
 * <?php } ?>
 * <a href="<?= url('/product/' . $pro['slug']) ?>">
 * <img src="<?= $pro['image'] ?>" class="img-fluid d-block mx-auto"/>
 * </a>
 * </div>
 * <div class="col-9 pr-0 related_product_description">
 * <h5><a href="<?= url('/product/' . $pro['slug']) ?>"
 * class="text_black"><?= $pro['name'] ?></a></h5>
 * <p>
 * <?php if ($pro['sale_price']) { ?>
 * <del class="text_gray"><?= format_money($pro['price']) ?></del>
 * <span class="text_black"><?= format_money($pro['sale_price']) ?></span>
 * <?php } else { ?>
 * <span class="text_black"><?= format_money($pro['price']) ?></span>
 * <?php } ?>
 * </p>
 * <div class="btn_wrapper">
 * <a href="<?= url('/product/' . $pro['slug']) ?>" class="theme_btn text-white">
 * BUY NOW</a>
 * </div>
 * </div>
 * </div>
 * </div>
 * <?php } ?>
 * </div>
 * </div>
 * </section>
 * <?php }  ?>
 * <?php **/ ?>



