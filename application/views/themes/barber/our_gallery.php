<?php $theme_view('includes/head'); ?>
<?php $theme_view('includes/headEnd'); ?>
<?php $theme_view('includes/header'); 
?>

	<div class="mainSection jarallax" id="product">
		<div class="container">
			<div class="section-head">
		
	<div class="gallerySection" id="gallery">
		<div class="container">
			<div class="row">
				<div class="col-xl-8 text-center mx-auto text-center">
					<div class="galleryTitlesSection">
						<h2>Our  Gallery</h2>
						<p>We've made a list of suggested activities based on your interests.Browse through our most popular
						Hotels!Our Featured Tours can help you find the trip that's perfect for you!.</p>
					</div><!-- /.rt-section-title-wrapper- -->
				</div><!-- /.col-lg-12 -->
			</div>

			<div class="row">
				<div class="col-12">
					<ul class="filterList">
						<li data-filter="*" class="active">All</li>
						<?php foreach ($gcategories as $gcat ){ if($gcat['count'] != 0){?>
							<li data-filter=".cg<?php echo esc($gcat['id'], true) ?>"><?php echo esc($gcat['cName'], true) ?></li>
						<?php }} ?>
					</ul>
				</div><!-- /.col-12 -->
			</div><!-- /.row -->
			<div class="row grid galleryLists">
				<?php foreach ($galleryImages as $gImage ){ ?>
				<div class="col-lg-3 col-md-6 grid-item cg<?php echo esc($gImage['catId'], true) ?>">
					<a class="d-block portfolioBox wow fade-in-bottom" href="<?php uploads('gallery/'.$gImage['imgPath']);?>" style="background-image: url(<?php uploads('gallery/'.$gImage['imgPath']);?>)">
						<div class="portfolioBoxOverlay"></div><!-- /.portfolioBoxOverlay -->
						<div class="portfolioBoxInnerContent">
							<h6><?php echo esc($gImage['imgName'], true) ?></h6>
							<p>
								<span><?php echo esc($gImage['imgDetails'], true); ?></span>
							</p>
						</div><!-- /.portfolioBoxInnerContent -->
					</a><!-- /.portfolioBox -->
				</div><!-- /.col-md-4 -->
				<?php } ?>
				
			</div><!-- /.row -->
			
		</div>
		<!-- /container -->
	</div>
	<!-- /gallerySection -->

	</div>
	<!-- /mainSection -->
	<?php $theme_view('includes/footer'); ?>
<?php $theme_view('includes/foot'); ?>

<?php $theme_view('includes/footEnd'); ?>