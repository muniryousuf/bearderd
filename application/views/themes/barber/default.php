<?php $theme_view('includes/head'); ?>
<link rel="stylesheet" href="<?php $assets('plugins/countries/build/css/intlTelInput.css'); ?>">
<?php $theme_view('includes/headEnd'); ?>
<?php $theme_view('includes/header'); ?>

	<div class="mainSection" id="home">
		<div class="home-slider">



			<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
				  <div class="carousel-inner">
				    <div class="carousel-item active">
				      <a href="<?php echo base_url('booking'); ?>" target="_blank">
				      		<img src="<?php $assets('images/barbar-banner1.png'); ?>" class="d-block w-100" alt="barbar-banner-design-01" border="0">
				      </a>
				    </div>
				    <div class="carousel-item">
				      <a href="<?php echo base_url('jobs'); ?>" target="_blank">

				      <img src="<?php $assets('images/barbar-banner2.png'); ?>" class="d-block w-100" alt="barbar-banner-design-02" border="0">
				  </a>
				    </div>
				    <div class="carousel-item">
				      <a href="<?php echo base_url('my_product'); ?>" target="_blank">

				      <img src="<?php $assets('images/barbar-banner3.png'); ?>" class="d-block w-100" alt="barbar-banner-design-03" border="0">
				  	</a>
				    </div>
				  </div>
				  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
			</div>
		</div>
		<div class="<?php echo_if($ads['top']['status'], 'p-t-40') ?>">
			<?php $theme_view('includes/top-ad') ?>
		</div>
	</div>
	<!-- /mainSection -->

<!--	<div class="servicesSec" id="home">-->
<!--		<div class="container">-->
<!--			<div class="ser-slider">-->
<!--				<div id="carouselServices" class="carousel slide" data-ride="carousel">-->
<!--					  <div class="carousel-inner">-->
<!--					    <div class="carousel-item active">-->
<!--					    	<div class="slider-box">-->
<!--						      <div class="img">-->
<!--						      	<img src="https://i.ibb.co/5FYr88S/sr-1.png">-->
<!--						      </div>-->
<!--						      <div class="sr-text">-->
<!--						      	<h3>Who are we? The Bearded Barber Club – Birmingham</h3>-->
<!--						      	<p>-->
<!--We are a brand-new modern barber shop which has taken tips from a classic barbershop and bought them into the modern 20th century. We have a range of skilled barbers who have experience with all types of hair. We offer refreshing cold beverages as well as premium hot drinks with all services.</p>-->
<!--						      	<a href="#" class="btn">-->
<!--						      		<span>Book Now</span></a>-->
<!--						      </div>-->
<!--					     	</div>-->
<!--					    </div>-->
<!--					    <div class="carousel-item">-->
<!--					      <div class="slider-box">-->
<!--						      <div class="img">-->
<!--						      	<img src="https://i.ibb.co/Z1gDbZZ/sr-2.png">-->
<!--						      </div>-->
<!--						      <div class="sr-text">-->
<!--						      	<h3>Who are we? The Bearded Barber Club – Birmingham</h3>-->
<!--						      	<p>We are a brand-new modern barber shop which has taken tips from a classic barbershop and bought them into the modern 20th century. We have a range of skilled barbers who have experience with all types of hair. We offer refreshing cold beverages as well as premium hot drinks with all services.</p>-->
<!--						      	<a href="#" class="btn">-->
<!--						      		<span>Book Now</span></a>-->
<!--						      </div>-->
<!--					     	</div>-->
<!--					    </div>-->
<!--					  </div>-->
<!--					  <a class="carousel-control-prev" href="#carouselServices" role="button" data-slide="prev">-->
<!--					    <span class="carousel-control-prev-icon" aria-hidden="true"></span>-->
<!--					    <span class="sr-only">Previous</span>-->
<!--					  </a>-->
<!--					  <a class="carousel-control-next" href="#carouselServices" role="button" data-slide="next">-->
<!--					    <span class="carousel-control-next-icon" aria-hidden="true"></span>-->
<!--					    <span class="sr-only">Next</span>-->
<!--					  </a>-->
<!--				</div>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
	<div class="aboutSec">
		<div class="container">
			<div class="about-area">
				<div class="title">
					<h3>Who We Are</h3>
					<p>Bearded Barber Club established in 2020, taking barbering to the next level. </p>
				</div>
				<div class="img-bx">
					<img src="https://i.ibb.co/4V82Scv/imggg.png">
				</div>
				<p>We have bought back the elements of traditional barbering where our no1 priority is the quality of our haircuts rather then the focusing on quantity. We operate our shop in a friendly relaxed environment where we offer a range of refreshing beverage including premium hot drinks.
Located in the heart of Birmingham city centre a few minutes’ walk from Broad street.  We offer both a walk-in and appointment services. Our team of barbers have a vast amount of knowledge in the barbering industry. We offer a range of different services, including hot towel shaves, facial waxing and more. Your hair/beard will be good hand when you visit us.</p>
			</div>
			<div class="openyours">
				<h3>OPEN YOUR OWN Bearded</h3>
				<p>	Opportunity awaits at   <span>Bearded</span></p>
				<a href="<?php echo base_url('contact_us'); ?>" target="_blank" class="btn formSubmitBtn btn1">
					<span>Find Out More</span></a>
			</div>
		</div>
	</div>
<!--	<div class="subscribe-sec">-->
<!--		<div class="container">-->
<!--			<p><span>SUBSCRIBE TO Bearded MAILING LIST HERE</span> <a href="#" class="btn formSubmitBtn btn1">-->
<!--				<span>Subscribe</span></a></p>-->
<!--		</div>	-->
<!--	</div>-->


	<section class="my-product home-product-sec">
		<div class="container">
			<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="section-heading-two">
							<h2>Products</h2>
							<p>You can book service from the cart view easily.</p>
						</div>
					</div>
				</div>
			<div id="carouselProducts" class="carousel slide" data-ride="carousel">
				  <div class="carousel-inner">
				    <div class="carousel-item active">
				      <div class="row pb-4 pr-4 pl-4">
                       <?php foreach ($productList as $key => $product ){   ?>
						<div class="col-md-4">
							<div class="product-box">
								<div class="img-box">
                                    <img src="<?php $assets($product['image']); ?>">
								</div>
								<div class="product-dis">
                                    <h3><?php echo esc($product['name'], true)?></h3>
                                    <p><?php echo esc($product['short_description'], true)?></p>
                                    <span>£ <?php echo esc($product['price'], true)?></span>
									<a href="<?php anchor_to('/product/'.$product['slug']) ?>" class="btn formSubmitBtn btn1">
										<span>Details</span>
									</a>


								</div>
							</div>
						</div>
                          <?php if($key ==2) break; }?>

					  </div>
				    </div>

				  </div>
				  <a class="carousel-control-prev" href="#carouselProducts" role="button" data-slide="prev">
				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="carousel-control-next" href="#carouselProducts" role="button" data-slide="next">
				    <span class="carousel-control-next-icon" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
			</div>
		</div>
	</section>

	<div class="testimonials-sec">
		<div class="container">
			<div id="carouselTestimonials" class="carousel slide" data-ride="carousel">
			  <ol class="carousel-indicators">
			    <li data-target="#carouselTestimonials" data-slide-to="0" class="active"></li>
			    <li data-target="#carouselTestimonials" data-slide-to="1"></li>
			    <li data-target="#carouselTestimonials" data-slide-to="2"></li>
			     <li data-target="#carouselTestimonials" data-slide-to="3"></li>
			  </ol>
			  <div class="carousel-inner">
			    <div class="carousel-item active">
			      <div class="carousel-caption d-md-block">
			        <p>First time visiting this new shop loved the service. I was well looked after from the time I walked in to the time I walked out with my fresh haircut. Highly recommend.</p>
			        <h5>John Hall</h5>
			      </div>
			    </div>
			    <div class="carousel-item">
			      <div class="carousel-caption d-md-block">
			        <p>Ravi did a great cut, will always come back as its local and top quality. </p>
			        <h5>Edward</h5>
			      </div>
			    </div>

			     <div class="carousel-item">
			      <div class="carousel-caption d-md-block">
			        <p>Top quality haircuts better then kings down the road. </p>
			        <h5>Saf</h5>
			      </div>
			    </div>

			    <div class="carousel-item">
			      <div class="carousel-caption d-md-block">
			        <p> Sohail took on board everything I said and blessed with an awesome haircut.</p>
			        <h5>Sam</h5>
			      </div>
			    </div>
			  </div>
			</div>
		</div>
	</div>
	<div class="signup-sec">
		<div class="container">
			<h3>SIGN UP</h3>
			<p>Like what we do, then be part of the crew.
subscribe to our mailing list and join the UK’s biggest barbershop group.</p>
			<form>
			  <div class="form-group">
			    <label for="exampleInputEmail1">Email address</label>
			    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your Email Address">
			  </div>
			  <button type="submit" class="btn btn-primary">Subscribe</button>
			</form>
		</div>
	</div>
	<!--About area start here-->
	<!-- <section class="about-area-one" id="ourServices">
		<div class="section-head">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="section-heading-two">
							<h2>Our Services</h2>
							<p>You can book service from the cart view easily.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<?php foreach ($serviceList as $servList ){ ?>
					<div class="col-md-6 col-xl-4 pd-0">
						<div class="about-list-right">
							<figure>
								<img src="<?php uploads("img/".$servList['image']);?>" alt=""/>
							</figure>
							<div class="content">
								<h4><?php echo esc($servList['title'], true)?></h4>
								<div class="serviceInfoMain">
									<div class="media mb-3">
										<i class="icon-clock serviceInfoicon"></i>
										<div class="media-body">
											<p class="serviceInfoHeading">Duration:</p>
											<p class="serviceInfoValue"><?php echo esc($servList['servDuration'], true)?></p>
										</div>
									</div>
									<div class="media mb-3">
										<i class="icon-coin-dollar serviceInfoicon"></i>
										<div class="media-body">
											<p class="serviceInfoHeading">Price/person:</p>
											<p class="serviceInfoValue">$<?php echo esc($servList['price'], true)?></p>
										</div>
									</div>
									<div class="media">
										<i class="icon-users serviceInfoicon"></i>
										<div class="media-body">
											<p class="serviceInfoHeading">Capacity/service:</p>
											<p class="serviceInfoValue"><?php echo esc($servList['servSpace'], true)?></p>
										</div>
									</div>
									<a href="#home" class="booknowBtn btn3" data-value="<?php echo esc($servList['id'], true)?>"><span>Book Now</span></a>
								</div>
							</div>
						</div>
					</div>
				<?php }?>
			</div>
		</div>
	</section> -->
	<!--About area end here-->

	<!--Team area start here-->

	<!--Team area end here-->
	<!--Blog area start here-->
	<?php if(isset($blogPosts)){ ?>
	<!-- <section class="blog-area section bg-img jarallax" id="blog">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="section-heading-three">
						<h2>News & Offers</h2>
						<p>Hair Salon was founded in 1996 with the mission of providing hair care services. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor.</p>
					</div>
				</div>
			</div>
			<div class="row">
				<?php foreach($blogPosts as $blogPost){ ?>
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="blog-list">
						<figure>
							<img src="<?php uploads('img/blog/'.$blogPost['image']); ?>" alt=""/>
							<div class="date">
								<?php $dateTimeUpdatedString = strtotime($blogPost['datetime_updated']); ?>
								<strong><?php echo date('j', $dateTimeUpdatedString); ?></strong>
								<span><?php echo date('M', $dateTimeUpdatedString); ?></span>
							</div>
						</figure>
						<div class="content">
							<h3><?php echo $blogPost['title']; ?></h3>
							<div class="blogDet"><?php echo word_limiter(esc($blogPost['description'], true), 17) ?></div>
							<a href="<?php echo base_url('blog/'.$blogPost['permalink']); ?>">Read More <i class="fa fa-angle-double-right"></i></a>
						</div>
					</div>
				</div>
				<?php } ?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="load-btn text-center">
						<a href="<?php echo base_url(BLOG_CONTROLLER); ?>" class="btn1 mr-t50"><span>View All</span></a>
					</div>
				</div>
			</div>
		</div>
	</section> -->
	<!--Blog area end here-->
	<?php } ?>






	<!--Booking area start here-->

	<!--Booking area end here-->

<?php $theme_view('includes/footer'); ?>
<?php $theme_view('includes/foot'); ?>
<script src="<?php $assets("plugins/moment/moment.min.js"); ?>"></script>

<script src="<?php $assets("js/default.js"); ?>"></script>
<?php $theme_view('includes/footEnd'); ?>
