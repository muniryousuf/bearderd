<?php $uri = strtolower($this->uri->segment(1)); ?>
<div class="covid-19">
		<div class="container">
            <a href="<?php echo base_url('page/covid'); ?>">
			<p>Covid 19</p>
            </a>
		</div>
	</div>
<header class="headerMain">
	<div class="headerMainInner">
		<nav class="navbar">
				<div class="main-header">
					<div class="container">
						<div class="logo">
						<a href="<?php echo base_url()?>" class="brandLogo"><img src="<?php $assets('images/logo.png'); ?>" alt="" width="100px"></a>
						<a href="<?php echo base_url()?>" class="stickyLogo"><img src="<?php $assets('images/logo.png'); ?>" alt="" width="50px"></a>
						</div>
						<div class="mainMenu">
							<ul id="myDIV">
								<li class="bdtn active"><a href="<?php echo base_url()?>">Home</a></li>
								<li class="bdtn"><a href="<?php echo base_url('booking'); ?>">Book Now</a></li>
								<li class="bdtn"><a href="<?php echo base_url('our_barber_shop'); ?>">barbershop</a></li>
								<li class="bdtn"><a href="<?php echo base_url('jobs'); ?>">Join the team</a></li>
								<!--<li class="bdtn"><a href="<?php echo base_url('#'); ?>">OUR BARBERSHOPS</a></li>-->
								<li class="bdtn"><a href="<?php echo base_url('blog'); ?>">Blog</a></li>
								<!--<li class="bdtn"><a href="<?php echo base_url('my_product'); ?>">Products</a></li>-->
<!--								<li class="bdtn"><a href="--><?php //echo base_url('blog'); ?><!--">Brainstorm </a></li>-->

                                <li class="bdtn"><a href="<?php echo base_url('my_product'); ?>">Shop </a></li>

								<li class="bdtn"><a href="<?php echo base_url('contact_us'); ?>">Contact us</a></li>

								<?php foreach($pages as $page) { if($page['status'] && ($page['position'] == 1 || $page['position'] == 3)) { ?>
									<li class="bdtn"><a href="<?php anchor_to('page/' . $page['permalink']) ?>"><?php echo esc($page['title'], true) ?></a></li>
								<?php } } ?>
							</ul>
							<div class="mobile-menu">
								<div class="menu-click">
									<span></span>
									<span></span>
									<span></span>
								</div>
							</div>

						</div>
						<div class="right">
							<ul class="social-icons">
								<li><a href="https://m.facebook.com/BeardedBarberClub/"><i class="fa fa-facebook"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter"></i></a></li>
								<li><a href="https://www.instagram.com/p/CH8jgGNlJxk/?igshid=1ba1e621ipnag"><i class="fa fa-instagram"></i></a></li>
							</ul>
                            <?php   $cart = $this->session->userdata('cart');
                            ?>
							<div class="cart-menu"><i class="fas fa-shopping-cart"></i> <a href="#"><span><?php echo count($cart) ?> item</span></a></div>
						</div>
					</div>
				</div>
				<!-- <div class="d-flex align-items-center main-nav">
					<div class="container">


					</div>
				</div> -->
				<div class="msg-bar">
					<div class="container">
                        <a href="<?php echo base_url('blog'); ?>">
                            <p>Covid 19 </p>
                        </a>
					</div>
				</div>
		</nav>
	</div>

</header>
