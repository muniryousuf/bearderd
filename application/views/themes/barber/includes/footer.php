<footer>
	<div class="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
					<div class="foo-about">
						<div class="foo-logo">
							<a href="#"><img src="<?php $assets('images/logo.png'); ?>" alt=""/></a>
						</div>
						<div class="content">
							<p><?php echo esc($contactdetails['address'], true) ?></p>
							<span><?php echo esc($contactdetails['phone'], true) ?></span>
							<p><?php echo esc($contactdetails['email'], true)?></p>
						</div>
						<ul class="list-inline">
							<li><a href="https://m.facebook.com/BeardedBarberClub/"><i class="fa fa-facebook"></i></a></li>
							<li><a href="<?php echo esc($contactdetails['urlTwt'], true) ?>"><i class="fa fa-twitter"></i></a></li>
							<li><a href="https://www.instagram.com/p/CH8jgGNlJxk/?igshid=1ba1e621ipnag"><i class="fa fa-instagram"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-sm-12 mb-4 col-xs-12">
					<div class="weight foo-link">
						<h3>Recommendations</h3>
						<ul>
							<li><a href="<?php echo base_url('#home'); ?>"><i class="fa fa-angle-double-right"></i><span>Home</span></a></li>
							<?php foreach($pages as $page) { if($page['status'] && $page['position'] >= 1) { ?>
								<li><a href="<?php anchor_to('page/' . $page['permalink']) ?>"><i class="fa fa-angle-double-right"></i><span><?php echo esc($page['title'], true) ?></span></a></li>
							<?php } } ?>
							<li><a href="<?php echo base_url('contact_us'); ?>"><i class="fa fa-angle-double-right"></i><span>Contact Us</span></a></li>
							<?php if($blogStatus['bstatus'] == 1){ ?>
								<li><a href="<?php echo base_url(BLOG_CONTROLLER); ?>"><i class="fa fa-angle-double-right"></i><span>Blog</span></a></li>
							<?php } ?>
							<li><a href="<?php echo base_url('booking'); ?>"><i class="fa fa-angle-double-right"></i><span>Book Now</span></a></li>

							<li><a href="<?php echo base_url('my_product'); ?>"><i class="fa fa-angle-double-right"></i><span>Shop</span></a></li>
								
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12">
				    <div class="openinghourscontent weight">
				        <div class="header">
				             <h3>Opening hours</h3>
				        </div>
				        <table class="opening-hours-table foo-link">
				            <tr>
				                <td>Monday</td>
				                <td class="shop-close">CLOSED</td>
				            </tr>
				            <tr>
				                <td>Tuesday</td>
				                <td class="opens">10:00 - 06:00 PM</td>
				            </tr>
				            <tr>
				                <td>Wednesday</td>
				                <td class="opens">10:00 - 06:00 PM</td>
				            </tr>
				            <tr>
				                <td>Thursday</td>
				                <td class="opens">10:00 - 06:00 PM</td>
				            </tr>
				            <tr>
				                <td>Friday</td>
				                <td class="opens">10:00 - 07:00 PM</td>
				            </tr>
				            <tr>
				                <td>Saturday</td>
				                <td class="opens">10:00 - 07:00 PM</td>
				            </tr>
				            <tr>
				                <td>Sunday</td>
				                <td class="shop-close">CLOSED</td>
				            </tr>
				        </table>					   
				    </div>
				</div>
					<div class="col-md-3 col-sm-12 col-xs-12 mt-3">
					<iframe src="<?php echo esc($contactdetails['map_src'], true) ?>" width="<?php echo esc($contactdetails['map_wd'], true) ?>" height="410" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
				</div>
			</div>
			
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="top-link-button text-center">
						<a id="totop" href="javascript:void(0)"><i class="fa fa-angle-up"></i></a>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="copyright text-center">
						<p>Copyright © <?php echo date('Y') ?>. All Rights Reserved By <a href="<?php echo base_url() ?>"><?php echo esc($general['title']) ?></a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<!--Footer area end here -->