<?php $theme_view('includes/head'); ?>
<?php $theme_view('includes/headEnd'); ?>
<?php $theme_view('includes/header'); ?>



<div class="cart-wrap" style="margin-top: 50px">
    <div class="container">
        <div class="row" style="margin-top: 20px !important;">
            <?php $subTotal = 0; ?>
            <div class="col-lg-8" style="margin-top: 30px">
                <?php if(count($cartData['items'])== 0) { ?>
                <div class="alert alert-danger" role="alert">
                    No Record Found
                </div>

                <?php }  else {?>
                <div class="main-heading">Shopping Cart</div>
                <div class="table-cart">
                    <table>
                        <thead>
                        <tr>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr  <?php foreach ($cartData['items'] as $value) { ?>>
                            <td>
                                <div class="display-flex align-center">
                                    <div class="img-product">
                                        <img src="<?php $assets($value['image']); ?>" alt="" class="mCS_img_loaded">
                                    </div>
                                    <div class="name-product">
                                        <?php echo  $value['product_name'] ?>
                                    </div>

                                </div>
                            </td>
                            <td>
                                <div class="price">
                                    £  <?php echo  $value['price'] ?>
                                </div>
                            </td>
                            <td class="product-count">
                                <div class="price">
                                    <?php echo  $value['quantity'] ?>
                                    <!--<input type="text" name="quantity" value="<?php /*echo  $value['quantity'] */?>" class="qty">-->
                                </div>
                            </td>
                            <td>
                                <div class="total">
                                    £ <?php echo  $value['price'] * $value['quantity'];

                                          $subTotal+=   $value['price'] * $value['quantity']
                                    ?>
                                </div>
                            </td>
                            <td>
                                <a href="#" title="">
                                    <img src="<?PHP echo  $value['image'] ?>" alt="" class="mCS_img_loaded">
                                </a>
                            </td>
                        </tr>
                        <?php  } ?>

                        </tbody>
                    </table>
                </div >

                <!-- /.table-cart -->
            </div>
            <!-- /.col-lg-8 -->
            <div class="col-lg-4">
                <div class="cart-totals">
                    <h3>Total Shopping Cart</h3>
                    <form action="#" method="get" accept-charset="utf-8">
                        <table>
                            <tbody>
                            <tr>
                                <th>Subtotal</th>
                                <td class="subtotal">£ <?php echo  $subTotal; ?></td>
                            </tr>
                            <tr>
                                <th>Delivery Fees</th>
                                <td class="free-shipping"> 0 </td>
                            </tr>
                            <tr class="total-row">
                                <th>Total</th>
                                <td class="price-total">£ <?php echo  $subTotal;?></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="btn-cart-totals">
                            <a href="<?= base_url('/checkout') ?>" class="checkout round-black-btn" title="">Continue to Checkout</a>
                        </div>
                        <!-- /.btn-cart-totals -->
                    </form>
                    <!-- /form -->
                </div>
                <!-- /.cart-totals -->
            </div>
            <?php } ?>

            <!-- /.col-lg-4 -->
        </div>
    </div>
</div>



<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&display=swap" rel="stylesheet">
<style>
    .cart-wrap {
        padding: 40px 0;

    }
    .cart-wrap *{
        font-family: 'Open Sans', sans-serif;
    }
    .main-heading {
        font-size: 19px;
        margin: 10px 0;
    }
    .table-cart table {
        width: 100%;
    }
    .table-cart thead {
        border-bottom: 1px solid #e5e5e5;
        border-top	: 1px solid #e5e5e5;
        margin-bottom: 5px;
    }
    .table-cart thead tr th {
        padding: 12px 0;
        color: #484848;
        font-size: 15px;
        font-weight: 700;
    }
    .table-cart tr{
        border-bottom: 1px solid #e5e5e5;
    }
    .table-cart tr td {
        padding: 10px 0;
        vertical-align: middle;
    }
    .table-cart tr td:nth-child(1) {
        /* width: 52%; */
    }
    .table-cart tr td:nth-child(2) {
        /* width: 26%; */
    }
    .table-cart tr td:nth-child(3) {
        /* width: 13.333%; */
    }
    .table-cart tr td:nth-child(4) {
        /* width: 8.667%; */
        /* text-align: right; */
    }
    .table-cart tr td .img-product {
        width: 72px;
        float: left;
        margin-left: 8px;
        margin-right: 31px;
        line-height: 63px;
    }
    .table-cart tr td .img-product img {
        width: auto;
        height:60px;
    }
    .table-cart tr td .name-product {
        font-size: 15px;
        color: #484848;
        /* padding-top: 8px; */
        /* line-height: 24px; */
        width: 50%;
    }
    .table-cart tr td .price {
        text-align: left;
        color: #333;
        font-size: 16px;
    }
    .table-cart tr td .quanlity {
        position: relative;
    }
    .product-count .qtyminus,
    .product-count .qtyplus {
        width: 34px;
        height: 34px;
        background: transparent;
        text-align: center;
        font-size: 19px;
        line-height: 34px;
        color: #000;
        cursor: pointer;
        font-weight: 600;
    }
    .product-count .qtyminus {
        line-height: 32px;
    }
    .product-count .qtyminus {
        border-radius: 3px 0 0 3px;
    }
    .product-count .qtyplus {
        border-radius: 0 3px 3px 0;
    }
    .product-count .qty {
        width: 60px;
        text-align: center;
        border: none;
    }
    .count-inlineflex {
        display: inline-flex;
        border: solid 2px #ccc;
        border-radius: 20px;
        margin:0;
    }
    .total {
        font-size: 18px;
        font-weight: 600;
        color: #333;
    }
    .display-flex {
        display: flex;
    }
    .align-center {
        align-items: center;
    }

    .coupon-box {
        padding: 15px 0;
        text-align: center;
        border: 2px dotted #e5e5e5;
        border-radius: 10px;
        margin-top: 25px;
        margin-bottom: 25px;
    }
    .coupon-box form input {
        display: inline-block;
        width: 264px;
        margin-right: 13px;
        height: 44px;
        border-radius: 25px;
        border: solid 2px #cccccc;
        padding: 5px 15px;
        font-size: 14px;
    }
    input:focus {
        outline: none;
        box-shadow: none;
    }
    .round-black-btn {
        border-radius: 0px;
        background: #ddd;
        color: #000;
        padding: 3px 35px;
        display: inline-block;
        border: solid 2px #212529;
        transition: all 0.5s ease-in-out 0s;
        cursor: pointer;
    }
    .round-black-btn:hover,
    .round-black-btn:focus {
        background: transparent;
        color: #212529;
        text-decoration: none;
    }
    .cart-totals {
        border-radius: 3px;
        background: #e7e7e74a;
        padding: 20px 15px;
        /* height:100%; */
    }
    .cart-totals h3 {
        font-size: 19px;
        color: #3c3c3c;
        letter-spacing: 1px;
        font-weight: 800;
        text-transform:uppercase;
    }
    .cart-totals table {
        width: 100%;
    }
    .cart-totals table tr{

        border-top:1px solid #e5e5e5;
    }
    .cart-totals table tr th,
    .cart-totals table tr td {
        /* width: 60%; */
        padding: 5px 0;
        vertical-align: middle;
        font-size:14px;
    }
    .cart-totals table tr td:last-child {
        /* text-align: right; */
    }
    .cart-totals table tr td.subtotal {
        /* font-size: 20px; */
        color: #6f6f6f;
    }
    .cart-totals table tr td.free-shipping {
        /* font-size: 14px; */
        color: #6f6f6f;
    }
    .cart-totals table tr.total-row td {
        /* padding-top: 25px; */
    }
    .cart-totals table tr td.price-total {
        /* font-size: 24px; */
        font-weight: 600;
        color: #333;
    }
    .cart-totals table tr th{
        font-weight: 600;
        padding: 10px 20px 10px 0px;
    }
    .cart-totals table tr td{
        text-align:left;
    }
    .btn-cart-totals {
        text-align: center;
        margin-top: 60px;
        margin-bottom: 20px;
    }
    .btn-cart-totals .round-black-btn {
        margin: 10px 0;
    }
</style>
<?php $theme_view('includes/footer'); ?>
<?php $theme_view('includes/foot'); ?>
<?php $theme_view('includes/footEnd'); ?>
