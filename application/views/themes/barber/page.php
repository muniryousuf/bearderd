<?php $theme_view('includes/head'); ?>
<?php $theme_view('includes/headEnd'); ?>
<?php $theme_view('includes/header'); ?>

<div class="breadcumb-area jarallax"></div>
    <section class="blog-area-page section">

        <div class="<?php echo_if($ads['top']['status'], 'p-b-40') ?>">
			<?php $theme_view('includes/top-ad') ?>
        </div>

		<div class="container">
            <div class="section-heading-one text-center">
                <h2><?php echo esc($page['title'], true) ?></h2>
            </div>
            <div class="pagesText">
                <?php echo esc($page['content'], true); ?>
            </div>
        </div>

        <div class="<?php echo_if($ads['bottom']['status'], 'p-t-25') ?>">
			<?php $theme_view('includes/bottom-ad') ?>
        </div>

    </section>
	<!-- /section -->

<?php $theme_view('includes/footer'); ?>
<?php $theme_view('includes/foot'); ?>

<?php $theme_view('includes/footEnd'); ?>
