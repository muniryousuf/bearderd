<?php $theme_view('includes/head'); ?>
<link rel="stylesheet" href="<?php $assets('plugins/countries/build/css/intlTelInput.css'); ?>">
<?php $theme_view('includes/headEnd'); ?>
<?php $theme_view('includes/header'); ?>
	<div class="mainSection jarallax contact-us booking" id="booking">
		<div class="container">
		<div class="row">
				<div class="col-lg-12">
					<!--<h1>Hair & <br>Beauty</h1>-->
                    <br>
                    <br>
					<p style="text-align: center">Due to covid 19 we are not taking bookings .</p>
					<!-- /selectionBoxMain -->
				</div>
			</div>
		</div>
	</div>
	<!-- /mainSection -->



	
	<!--Booking area start here-->
	
	<!--Booking area end here-->

<?php $theme_view('includes/footer'); ?>
<?php $theme_view('includes/foot'); ?>
<script src="<?php $assets("plugins/moment/moment.min.js"); ?>"></script>
<script src="<?php $assets("plugins/datepicker/bootstrap-datetimepicker.min.js"); ?>"></script>
<script src="<?php $assets("js/magnific.popup.min.js"); ?>"></script>
<?php if($stripe['status'] == 1 && $stripe['stripe_publishable_key'] && $stripe['stripe_api_key']){ ?>
<script src="https://js.stripe.com/v3/"></script>
<script>
	// Stripe Publishable Key.
	var stripe = Stripe('<?php echo $stripe['stripe_publishable_key']; ?>');
</script>
<?php } ?>
<?php if($paypal['status'] == 1 && $paypal['clientId'] && $paypal['clientSecret']){ ?>
	<script src="https://www.paypal.com/sdk/js?client-id=<?php echo $paypal['clientId']; ?>&currency=EUR&disable-funding=credit,card"></script>
<?php } ?>
<?php if(!isset($userinfo['phone']) || empty($userinfo['phone'])){ ?>
	<script src="<?php $assets("plugins/countries/prism.js"); ?>"></script>
	<script src="<?php $assets("plugins/countries/build/js/intlTelInput.js"); ?>"></script>
<?php } ?>
<script src="<?php $assets("js/default.js"); ?>"></script>
<?php $theme_view('includes/footEnd'); ?>