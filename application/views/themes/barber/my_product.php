<?php $theme_view('includes/head'); ?>
<?php $theme_view('includes/headEnd'); ?>
<?php $theme_view('includes/header'); ?>
	<div class="mainSection jarallax" id="product">
			<div class="section-head">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<div class="section-heading-two">
								<h2>Products</h2>
							</div>
						</div>
					</div>
					<div class="row pb-4 pr-4 pl-4">

						 <?php foreach ($productList as $product ){ ?>
						    <div class="col-md-4">
							<div class="product-box">
								<div class="img-box">
									<img src="<?php $assets($product['image']); ?>">
								</div>
								<div class="product-dis">
									<h3><?php echo esc($product['name'], true)?></h3>
									<p><?php echo esc($product['short_description'], true)?></p>
									<span>£ <?php echo esc($product['price'], true)?></span>
									<a href="<?php anchor_to('/product/'.$product['slug']) ?>" class="btn formSubmitBtn btn1 view-details-btn">View Details
                                     >

                                    </a>
								</div>
							</div>
						</div>
						<?php }?>

					</div>
		    	</div>
			</div>
	</div>
	<!-- /mainSection -->
	<?php $theme_view('includes/footer'); ?>
<?php $theme_view('includes/foot'); ?>

<?php $theme_view('includes/footEnd'); ?>
