<?php $theme_view('includes/head'); ?>
<?php $theme_view('includes/headEnd'); ?>
<?php $theme_view('includes/header'); ?>

    <!-- <div class="breadcumb-area jarallax"></div> -->
    <section class="blog-area-page section">

        <div class="<?php echo_if($ads['top']['status'], 'p-b-40') ?>">
			<?php $theme_view('includes/top-ad') ?>
        </div>

        <div class="container">
            <!-- <div class="row">
                <?php foreach($blogList as $blist){ if($blist['status'] == 1){ ?>
                    <div class="col-lg-4 col-md-6">
                        <div class="blog-list">
                            <figure>
                                <a href="<?php anchor_to(BLOG_CONTROLLER.'/'.$blist['permalink']) ?>">
                                    <img src="<?php uploads('img/blog/'.$blist['image']); ?>" alt=""/>
                                </a>
                                <div class="date">
                                    <?php $dateTimeUpdatedString = strtotime($blist['datetime_updated']); ?>
                                    <strong><?php echo date('j', $dateTimeUpdatedString); ?></strong>
                                    <span><?php echo date('M', $dateTimeUpdatedString); ?></span>
                                </div>
                            </figure>
                            <div class="content">
                                <h3><a href="<?php anchor_to(BLOG_CONTROLLER.'/'.$blist['permalink']) ?>"><?php echo word_limiter(esc($blist['title'], true), 8) ?></a></h3>
                                <div><?php echo word_limiter(esc($blist['description'], true), 17) ?></div>
                                <a href="<?php anchor_to(BLOG_CONTROLLER.'/'.$blist['permalink']) ?>">Read More <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>
                <?php } } ?>
                <div class="col-12">
                    <?php echo  $this->pagination->create_links(); ?>
                </div>
            </div> -->
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="section-heading-two">
                        <h2>Blog</h2>
                    </div>
                </div>
            </div>
            <?php foreach($blogList as $blist){ if($blist['status'] == 1){ ?>
            <div class="blog-item row mt-5 ">
                <div class="img-box col-12 col-lg-4 mb-3">
                    <img src="<?php $assets ('images/gallery/img7.png'); ?>">
                </div>
                <div class="post-info col-12 col-lg-8">
                   <h3><?php echo word_limiter(esc($blist['title'], true), 8) ?></h3>
                   <p><?php echo word_limiter(esc($blist['description'], true), 25) ?><</p>
                   <a href="<?php anchor_to(BLOG_CONTROLLER.'/'.$blist['permalink']) ?>">Read More</a>
                </div>
            </div>
            <?php } } ?>

        </div>

        <div class="<?php echo_if($ads['bottom']['status'], 'p-t-25') ?>">
			<?php $theme_view('includes/bottom-ad') ?>
		</div>

    </section>

<?php $theme_view('includes/footer'); ?>
<?php $theme_view('includes/foot'); ?>

<?php $theme_view('includes/footEnd'); ?>
