<?php $theme_view('includes/head'); ?>
<link rel="stylesheet" href="<?php $assets('plugins/countries/build/css/intlTelInput.css'); ?>">
<?php $theme_view('includes/headEnd'); ?>
<?php $theme_view('includes/header'); ?>
	<div class="mainSection jarallax booking" id="booking">
		<div class="container">
		<div class="row">
				<div class="col-lg-12">
					<!--<h1>Hair & <br>Beauty</h1>-->
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="section-heading-two">
							<h2>Booking</h2>
						</div>
					</div>

					<?php if($added = $this->session->flashdata('added')){
						$added_class = $this->session->flashdata('added_class');
					?>
						<div class="alert <?php echo esc($added_class, true);?>"><?php echo esc($added, true);?></div>
					<?php
					}
					?>
					<div class="selectionBoxMain">
						<form id="serviceUserForm" method="post" action="<?php echo base_url('homepage/submitData');?>">

						<div class="formDataMain">
							<div class="row">
								<div class="col-lg-12">
									<label>Please Select  Services</label>
									<div class="services-multiselect text-center" data-toggle="buttons">
								        <?php foreach ($serviceList as $servList ){ ?>

                                        <label class="btn btn-default">
                                            <input type="checkbox"   id="selectBookNow" name="serviceTitle" value="<?php echo esc($servList['id'], true)?>">
                                            <div><?php echo esc($servList['title'], true)?></div>
                                        </label>
                                    <?php }?>

								    </div>
								</div>
							</div>
							<!--<div class="singleInput form-group">-->
							<!--	<select class="custom-select selectBookNow" id="selectBookNow" name="serviceTitle" required>-->
							<!--		<option value="">Select Option</option>-->
							<!--		<?php foreach ($serviceList as $servList ){ ?>-->
							<!--			<option value="<?php echo esc($servList['id'], true)?>" label="<?php echo esc($servList['title'], true)?>"><?php echo esc($servList['title'], true)?></option>-->
							<!--		<?php }?>-->
							<!--	</select>-->
							<!--	<span class="iconbadge"><span class="icon-shop"></span></span>-->
							<!--	<span class="iconArrow"><span class="icon-cheveron-down"></span></span>-->
							<!--</div>-->
							<small id="sTitleError" class="form-text text-danger d-none">Please select One Service atleast.</small>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<label for="">Adults</label>
										<input id="adultsVal" disabled data-attr="" type="number" value="0" min="1" step="1" name="serviceAdult"/>
									</div>
								</div>
								<!--<div class="col-lg-6">-->
								<!--	<div class="form-group">-->
								<!--		<label for="">Childrens</label>-->
										<input id="childVal" disabled data-attr="" type="hidden" value="0" min="0" step="1" name="serviceChildren"/>
								<!--	</div>-->
								<!--</div>-->
							</div>
							<div class="row">
								<div class="col-lg-6">
									<label for="">Select Date</label>
									<div class="calendarClickMenu singleInput form-group">
										<div class="calendarInnerText"><i class="icon-calendar2"></i>
											<input type="text" placeholder="Select Date" class="form-control dateTimePickerInput" id="datepicker" disabled="disabled" name="serviceDate" required>
										</div>
									</div>
									<small id="dateError" class="form-text text-danger d-none">Please select Date.</small>
									<small id="notimeError" class="form-text text-danger d-none">No time available for today. Select Other Date.</small>
								</div>
								<div class="col-lg-6">
									<label for="">Select Time</label>
									<div class="singleInput form-group">
										<select class="custom-select" id="userSelectTiming" name="serviceTiming" disabled="disabled" required>
											<option value="0">Select Time</option>
										</select>
										<span class="iconbadge"><span class="icon-time"></span></span>
										<span class="iconArrow"><span class="icon-cheveron-down"></span></span>
									</div>
									<small id="timeError" class="form-text text-danger d-none">Please select Time.</small>
								</div>
							</div>
							<label id="selectAgentLabel" class="d-none">Select Agent</label>
							<div class="selectAgentMain"></div>
							<div class="btn btn-dark formSubmitBtn disabled" disabled="disabled" id="formContinue">Continue</div>
						</div>
						<!-- /formDataMain -->
						<div class="alert alert-info" id="alreadyBooked" style="display:none">Already booked this service on this Date/Time.</div>
						<div class="afterContinueMain d-none">
							<div class="afterContinue">
								<img class="serviceImg" src="" alt="">
								<ul class="serviceDetail">
									<li><h5><span id="serviceTitle"></span> </h5></li>

									<li><span class="labels">Agent Name:</span> <span id="agentName"></span></li>
									<li><span class="labels">Date:</span> <span id="selectedDate"></span></li>
									<li><span class="labels">Time:</span> <span id="selectedTime"></span></li>
									<li><span class="labels">No of guest:</span> <span id="selectedAdults"></span> Adults - <span id="selectedChildrens"></span> Children</li>
									<li><span class="labels">Service Price:</span> £<span id="servicePersonPrice"></span></li>
									<li><span class="labels">Total Price:</span> £<span id="serviceTotalPrice"></span></li>
								</ul>
							</div>
							<div class="btn btn-outline-danger btn-block m-t-15 formChange">Change Options</div>
							<hr>
							<?php if(!$this->session->userdata('id')){ ?>
								<div class="form-group">
									<label for="fullName">Username</label>
									<div class="singleInput">
										<input type="text" id="input-userFullName" class="form-control selectBookNow" placeholder="Username" name="userFullName">
									</div>
									<div id="error"></div>
								</div>
								<div class="form-group">
									<label for="userEmail">Email</label>
									<div class="singleInput">
										<input type="email" id="input-userEmail" class="form-control selectBookNow" placeholder="user@email.com" name="userEmail">
									</div>
									<div id="error"></div>
								</div>
								<div class="form-group">
									<label for="userPhone">Phone Number</label>
									<div class="singleInput">
										<input type="text" id="input-userPhone" class="form-control selectBookNow" placeholder="Type number without country code" name="userPhone">
									</div>
									<div id="error"></div>
									<span id="valid-msg" class="d-none form-text text-success">✓ Valid</span>
									<span id="error-msg" class="d-none form-text text-danger"></span>
									<span id="empty-msg" class="d-none form-text text-danger">Please fill valid phone number first.</span>
								</div>
							<?php } ?>
							<?php if($this->session->userdata('id') ){
								if(!isset($userinfo['phone']) || empty($userinfo['phone'])) {?>
								<div class="form-group">
									<label for="userPhone">Phone Number</label>
									<div class="singleInput">
										<input type="text" id="input-userPhone" class="form-control selectBookNow" placeholder="Type number without country code" name="userPhone">
									</div>
									<div id="error"></div>
									<span id="valid-msg" class="d-none form-text text-success">✓ Valid</span>
									<span id="error-msg" class="d-none form-text text-danger"></span>
									<span id="empty-msg" class="d-none form-text text-danger">Please fill valid phone number first.</span>
								</div>
							<?php }} ?>
							<!-- -->
							<label for="">Select Payment Method</label>
							<div class="singleInputform form-group">
								<select class="custom-select" name="selectPayment" id="selectMethod" required>
									<option value="0" selected>By Cash</option>
									<?php if($stripe['status'] == 1 && $stripe['stripe_publishable_key'] && $stripe['stripe_api_key']){ ?><option value="1">Credit Card</option><?php } ?>
									<?php if($paypal['status'] == 1 && $paypal['clientId'] && $paypal['clientSecret']){ ?><option value="2">Paypal</option><?php } ?>
								</select>
								<span class="iconbadge"><span class="icon-credit-card"></span></span>
								<span class="iconArrow"><span class="icon-cheveron-down"></span></span>
							</div>
							<div id="payment-card" data-val="1" class="d-none">
								<div id="card-element"><!-- A Stripe Element will be inserted here. --></div>
								<!-- Used to display form errors. -->
								<small id="card-errors" role="alert" class="form-text text-danger"></small>
							</div>
							<!-- -->
							<div id="payment-paypal" data-val="2" class="d-none">
								<span id="payWithPaypal" class="m-b-40"></span>
								<small id="paypal-errors" role="alert" class="form-text text-danger"></small>
							</div>
							<!-- -->

                            <input type="hidden" name="multiple-booking" id="multiple-booking" value="">
                            <button class="btn btn-dark formSubmitBtn btn-block" type="submit" id="serviceSubmit" name="submit">
								<span class="loaderBeforeSubmit">Submit</span>
								<div class="stage d-none"><div class="dot-floating"></div></div>
							</button>
						</div>



						<!-- /afterContinueMain -->
						</form>
					</div>
					<!-- /selectionBoxMain -->
				</div>
			</div>
		</div>
	</div>
	<!-- /mainSection -->




	<!--Booking area start here-->

	<!--Booking area end here-->

<?php $theme_view('includes/footer'); ?>
<?php $theme_view('includes/foot'); ?>
<script src="<?php $assets("plugins/moment/moment.min.js"); ?>"></script>
<script src="<?php $assets("plugins/datepicker/bootstrap-datetimepicker.min.js"); ?>"></script>
<script src="<?php $assets("js/magnific.popup.min.js"); ?>"></script>
<?php if($stripe['status'] == 1 && $stripe['stripe_publishable_key'] && $stripe['stripe_api_key']){ ?>
<script src="https://js.stripe.com/v3/"></script>
<script>
	// Stripe Publishable Key.
	var stripe = Stripe('<?php echo $stripe['stripe_publishable_key']; ?>');
</script>
<?php } ?>
<?php if($paypal['status'] == 1 && $paypal['clientId'] && $paypal['clientSecret']){ ?>
	<script src="https://www.paypal.com/sdk/js?client-id=<?php echo $paypal['clientId']; ?>&currency=EUR&disable-funding=credit,card"></script>
<?php } ?>
<?php if(!isset($userinfo['phone']) || empty($userinfo['phone'])){ ?>
	<script src="<?php $assets("plugins/countries/prism.js"); ?>"></script>
	<script src="<?php $assets("plugins/countries/build/js/intlTelInput.js"); ?>"></script>
<?php } ?>
<script src="<?php $assets("js/default.js"); ?>"></script>
<?php $theme_view('includes/footEnd'); ?>
