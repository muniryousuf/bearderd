	<?php $theme_view('includes/head'); ?>
<?php $theme_view('includes/headEnd'); ?>
<?php $theme_view('includes/header'); ?>
<div class="main-title-section mainSection" style="background: url('https://i.ibb.co/WG1PK8f/1.png'); background-size: cover; background-position: center;">
			<div class="container">
				<div class="row">
					<div class="col-12 col-lg-7">
						<h3>Contact Us</h3>
						<p>We'd be happy to help. <br>
							If you have an urgent enquiry, we'd love to hear from you!</p>
						<!-- <ul>
							<li>
								<a href="#" class="btn btn1">
									<span>JOBS/ CAREERS</span></a>
							</li>
							<li>
								<a href="#" class="btn formSubmitBtn btn2">
									<span>FRANCHISE</span></a>
							</li>
						</ul> -->
					</div>
				</div>
			</div>
			<div class="overlay"></div>
		</div>
	<div class="mainSection jarallax contact-us" id="contact">
		<div class="container">
			<div class="row">
                <div class="col-lg-5">
                	<div class="office-address">
                		<div class="heading">
								<!-- <h1>Get In Touch</h1> -->
								<p>Any kind of travel information don't hesitate to contact with us for imiditate customer support. We are love to hear from you.</p>
								<div class="content">
									<ul>
									    <li>Address: <a href="#"><?php echo esc($contactdetails['address'], true) ?></a></li>
									    
										<li>Phone: <a href="#"><?php echo esc($contactdetails['phone'], true) ?></a></li>
										<li>Email: <a href="#"><?php echo esc($contactdetails['email'], true)?></a></li>
									</ul>
								</div>
								<div class="alert alert-info" id="somethngwrng" style="display:none">Something wrong please try again.</div>
								<div class="alert alert-success" id="submitedEmail" style="display:none">Your message sent successfully. We will contact you in 48 hrs.</div>
							</div>
                	</div>
                </div>
                <div class="col-lg-7">
                	<div class="contact-form selectionBoxMain">

                		<form id="mailmesubmit" action="<?php echo base_url(HOMEPAGE_CONTROLLER.'/mailme') ?>" class="rt-form rt-line-form">
							<fieldset class="form-group">
								<input type="text" id="cForm-name" placeholder="Name (with no space)" name="name" class="customInputs form-control">
								<div id="error"></div>
							</fieldset>
							<fieldset class="form-group">
								<input type="email" id="cForm-email" placeholder="Email" name="email" class="customInputs form-control">
								<div id="error"></div>
							</fieldset>
							<fieldset class="form-group">
								<input type="email" id="cForm-email" placeholder="Subject" name="subject" class="customInputs form-control">
								<div id="error"></div>
							</fieldset>
					<!-- 		<fieldset class="form-group">
								<input type="time" id="appt" placeholder="Select Time" name="subject" class="customInputs form-control">
								<div id="error"></div>
							</fieldset>
							<fieldset class="form-group">
								<select class="custom-select custom-select-lg mb-3">
								  <option selected>Working Day</option>
								  <option value="1">Monday</option>
								  <option value="2">Tuesday</option>
								  <option value="3">Wednesday</option>
								  <option value="3">Thursday</option>
								  <option value="3">Friday</option>
								</select>
								<div id="error"></div>
							</fieldset> -->
							<fieldset class="form-group">
								<textarea placeholder="Message" id="cForm-message" rows="4" name="message" class="customTextarea form-control"></textarea>
								<div id="error"></div>
							</fieldset>
							<?php if($recaptcha['status']) { ?>
							<div class="form-group text-left">
								<div class="g-recaptcha" data-sitekey="<?php echo esc($recaptcha['site_key']) ?>"></div>
							</div>
							<?php } ?>
							<button type="submit" class="btn1 customFormButton">
								<span class="loaderBeforeC">Submit Now</span>
								<div class="loaderBeforeCg p-l-40 p-r-40 p-t-5 p-b-5 d-none"><div class="dot-floating"></div></div>
							</button>
						</form>
                	</div>
                </div>
			</div>
		</div>
	</div>
	<!-- /mainSection -->
	<?php $theme_view('includes/footer'); ?>
<?php $theme_view('includes/foot'); ?>

<?php $theme_view('includes/footEnd'); ?>