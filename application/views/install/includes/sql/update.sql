DROP PROCEDURE IF EXISTS `?`;
DELIMITER //
CREATE PROCEDURE `?`()
BEGIN
  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN END;
  ALTER TABLE `logintbl` ADD `blockStatus` tinyint(4) NOT NULL DEFAULT FALSE AFTER `privacy`;
END //
DELIMITER ;
CALL `?`();
DROP PROCEDURE `?`;

CREATE TABLE IF NOT EXISTS `twilio-settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` text NOT NULL,
  `token` text NOT NULL,
  `number` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

INSERT INTO `twilio-settings` (`id`, `sid`, `token`, `number`, `message`, `status`) VALUES
(1, '', '', '', '', 0);