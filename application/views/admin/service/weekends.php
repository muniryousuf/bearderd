<?php $this->load->view('admin/includes/head'); ?>
<div class="wrapper fullheight-side">
<?php $this->load->view('admin/includes/header');
$this->load->view('admin/includes/sidebar'); 
$this->load->view('admin/includes/navbar'); ?>

<!-- Page Content -->

<div class="main-panel">
    <div class="container">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?php echo esc($page_title) ?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?php anchor_to(GENERAL_CONTROLLER . '/dashboard') ?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-home">
                        <a href="<?php anchor_to(SERVICE_CONTROLLER . '/weekends') ?>">
                        <?php echo esc($page_title) ?>
                        </a>
                    </li>
                </ul>
            </div>
            <?php $this->load->view('admin/includes/alert'); ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <form method="POST" action="<?php anchor_to(SERVICE_CONTROLLER . '/weekends') ?>">
                            <div class="card-body">
                                <?php foreach($weekdays as $weekday){ ?>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" name="weekdays[]" value="<?php echo esc($weekday['id'], true) ?>" <?php if($weekday['status'] == '1') { ?>checked<?php } ?>>
                                            <span class="form-check-sign"><?php echo esc($weekday['weekdays'], true) ?></span>
                                        </label>
                                    </div>
                                <?php } ?>
                                <?php echo form_error('weekdays[]', '<br><span class="text-danger">', '</span>'); ?>
                            </div>
                            <div class="card-footer">
                                <div class="form-group text-left">
                                    <input type="hidden" name="submit" value="Submit">
                                    <button class="btn btn-success"><i class="fas fa-plus mr-1"></i> Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End Page Content -->

</div>
<?php $this->load->view('admin/includes/foot'); ?>
<?php $this->load->view('admin/includes/footEnd'); ?>