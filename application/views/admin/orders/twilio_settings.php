<?php $this->load->view('admin/includes/head'); ?>
<link rel="stylesheet" href="<?php public_assets('plugin/countries/build/css/intlTelInput.css'); ?>">
<div class="wrapper fullheight-side">
<?php $this->load->view('admin/includes/header');
$this->load->view('admin/includes/sidebar'); 
$this->load->view('admin/includes/navbar'); ?>

<div class="main-panel">
   <div class="container">
      <div class="page-inner">
         <div class="page-header">
            <h4 class="page-title"><?php echo esc($page_title) ?></h4>
            <ul class="breadcrumbs">
               <li class="nav-home">
                  <a href="<?php anchor_to(GENERAL_CONTROLLER . '/dashboard') ?>">
                  <i class="flaticon-home"></i>
                  </a>
               </li>
               <li class="separator">
                  <i class="flaticon-right-arrow"></i>
               </li>
               <li class="nav-home">
                  <a href="<?php anchor_to(PAYMENTS_CONTROLLER . '/twilio') ?>">
                  <?php echo esc($page_title) ?>
                  </a>
               </li>
            </ul>
         </div>
         <?php $this->load->view('admin/includes/alert'); ?>
         <div class="row">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="card-title">Update your twilio settings</div>
                  </div>
				  <form action="<?php anchor_to(PAYMENTS_CONTROLLER . '/twilio') ?>" method="POST">
					<div class="card-body">
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label for="sid">SID</label>
                           <?php echo form_error('sid', '<br><span class="text-danger">', '</span>') ?>
                           <input type="text" name="sid" class="form-control resize-none" id="sid" placeholder="Enter your twilio Client ID." value="<?php echo esc($twilio['sid']) ?>">
								</div>
                        <div class="form-group">
									<label for="token">Token</label>
                           <?php echo form_error('token', '<br><span class="text-danger">', '</span>') ?>
                           <input type="text" name="token" class="form-control resize-none" id="token" placeholder="Enter your twilio Client Secret." value="<?php echo esc($twilio['token']) ?>">
								</div>
                        <div class="form-group">
									<label for="number">Twilio Number</label>
                           <?php echo form_error('number', '<br><span class="text-danger">', '</span>') ?>
									<span id="valid-msg" class="d-none form-text text-success">✓ Valid</span>
									<span id="error-msg" class="d-none form-text text-danger"></span>
									<span id="empty-msg" class="d-none form-text text-danger">Please fill valid phone number first.</span>
                           <input type="text" name="number" class="form-control resize-none" id="input-userPhone" placeholder="Enter your twilio number." value="<?php echo esc($twilio['number']) ?>">
								</div>
                        <div class="form-group">
									<label for="message">Message</label>
                           <?php echo form_error('message', '<br><span class="text-danger">', '</span>') ?>
                           <input type="text" name="message" class="form-control resize-none" id="message" placeholder="Enter your message that you can send to user." value="<?php echo esc($twilio['message']) ?>">
								</div>
                        <div class="form-group">
                           <div class="custom-control custom-switch">
                              <input <?php echo_if($twilio['status'], 'checked') ?> name="site-status" type="checkbox" class="custom-control-input" id="switch">
                              <label class="custom-control-label" for="switch">Status</label>
                           </div>
                           <small>Twilio SID &amp; Twilio Token are <span class="text-danger">Required</span> if the status is turned on.</small>
								</div>
							</div>
						</div>
					</div>
					<div class="card-action">
						<input type="hidden" name="submit" value="Submit">
                  <button type="submit" class="btn btn-success"><i class="fas fa-check mr-1"></i> Update Settings</button>
                  <a target="_blank" href="https://www.twilio.com/" class="btn btn-info float-right"><i class="fas fa-external-link-alt mr-1"></i> Get your twilio Credentials</a>
				  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- End Page Content -->
</div>
<?php $this->load->view('admin/includes/foot'); ?>

<script src="<?php public_assets("plugin/countries/prism.js"); ?>"></script>
<script src="<?php public_assets("plugin/countries/build/js/intlTelInput.js"); ?>"></script>
<script src="<?php public_assets("plugin/countries/defaultCountries.js"); ?>"></script>
<?php $this->load->view('admin/includes/footEnd'); ?>