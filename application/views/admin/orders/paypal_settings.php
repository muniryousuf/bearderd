<?php $this->load->view('admin/includes/head'); ?>
<div class="wrapper fullheight-side">
<?php $this->load->view('admin/includes/header');
$this->load->view('admin/includes/sidebar'); 
$this->load->view('admin/includes/navbar'); ?>

<div class="main-panel">
   <div class="container">
      <div class="page-inner">
         <div class="page-header">
            <h4 class="page-title"><?php echo esc($page_title) ?></h4>
            <ul class="breadcrumbs">
               <li class="nav-home">
                  <a href="<?php anchor_to(GENERAL_CONTROLLER . '/dashboard') ?>">
                  <i class="flaticon-home"></i>
                  </a>
               </li>
               <li class="separator">
                  <i class="flaticon-right-arrow"></i>
               </li>
               <li class="nav-home">
                  <a href="<?php anchor_to(PAYMENTS_CONTROLLER . '/paypal') ?>">
                  <?php echo esc($page_title) ?>
                  </a>
               </li>
            </ul>
         </div>
         <?php $this->load->view('admin/includes/alert'); ?>
         <div class="row">
            <div class="col-md-12">
               <div class="card">
                  <div class="card-header">
                     <div class="card-title">Update your paypal settings</div>
                  </div>
				  <form action="<?php anchor_to(PAYMENTS_CONTROLLER . '/paypal') ?>" method="POST">
					<div class="card-body">
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label for="clientId">Client ID</label>
                           <?php echo form_error('clientId', '<br><span class="text-danger">', '</span>') ?>
                           <input type="text" name="clientId" class="form-control resize-none" id="clientId" placeholder="Enter your Paypal Client ID." value="<?php echo esc($paypal['clientId']) ?>">
								</div>
                        <div class="form-group">
									<label for="clientSecret">Client Secret</label>
                           <?php echo form_error('clientSecret', '<br><span class="text-danger">', '</span>') ?>
                           <input type="text" name="clientSecret" class="form-control resize-none" id="clientSecret" placeholder="Enter your Paypal Client Secret." value="<?php echo esc($paypal['clientSecret']) ?>">
								</div>
                        <div class="form-group">
                           <div class="custom-control custom-switch">
                              <input <?php echo_if($paypal['status'], 'checked') ?> name="site-status" type="checkbox" class="custom-control-input" id="switch">
                              <label class="custom-control-label" for="switch">Status</label>
                           </div>
                           <small>PayPal Client ID &amp; PayPal Client Secret are <span class="text-danger">Required</span> if the status is turned on.</small>
								</div>
							</div>
						</div>
					</div>
					<div class="card-action">
						<input type="hidden" name="submit" value="Submit">
                  <button type="submit" class="btn btn-success"><i class="fas fa-check mr-1"></i> Update Settings</button>
                  <a target="_blank" href="https://developer.paypal.com/" class="btn btn-info float-right"><i class="fas fa-external-link-alt mr-1"></i> Get your PayPal Credentials</a>
				  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- End Page Content -->
</div>
<?php $this->load->view('admin/includes/foot'); ?>
<?php $this->load->view('admin/includes/footEnd'); ?>