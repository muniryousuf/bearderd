<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Myproduct extends CI_Controller
{
    private $page_data;

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->model('ProductModel');

        $this->page_data = $this->MainModel->pageData();
        $this->all_products = $this->ProductModel->productList();
    }

    public function index($permalink = null)
    {
        $data = $this->all_products;

        $themeViewData = array_merge(
            $this->page_data,
            array(
                'productList' => $data
            )
        );

        $theme_view = $this->page_data['theme_view'];
        $theme_view('my_product', $themeViewData);
    }
}

?>