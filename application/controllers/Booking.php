<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {

    private $page_data;

    public function __construct() {
        parent::__construct();

        $this->load->database();
        $this->load->helper('text');
        $this->load->model('GalleryModel');
        $this->load->model('LayoutModel');
        $this->load->model('StripeModel');
        $this->load->model('PaypalModel');
        $this->load->model('OrderModel');
        $this->load->model('AgentsModel');
        $this->load->model('BlogModel');
        $this->load->model('BookingModel');

        $this->loginSession 			= $this->session->userdata('id');
        $this->page_data 				= $this->MainModel->pageData();
        $this->page_data['email'] 		= $this->MainModel->smtp_settings();
        $this->page_data['user'] 	    = $this->LoginModel->user_info($this->loginSession);
        $this->page_data['stripe'] 		= $this->OrderModel->getStripe();
        $this->page_data['paypal'] 		= $this->PaypalModel->getPaypal();
        $this->page_data['twilio'] 		= $this->OrderModel->getTwilio();
        $this->page_data['agentsL'] 	= $this->AgentsModel->agentsL();
        $this->page_data['blogPosts']   = $this->BlogModel->blogL();
        $this->StripeModel 				= $this->StripeModel->get();
        $this->validateSession 			= $this->LoginModel->validateSession($this->loginSession);
        $this->page_data['weekends'] 	= $this->ServiceModel->homepageWeekends();

        $this->load->library('stripe_lib', array(
            'stripe_api_key'     		=> $this->page_data['stripe']['stripe_api_key'],
            'stripe_publishable_key' 	=> $this->page_data['stripe']['stripe_publishable_key'],
            'stripe_currency' 			=> $this->page_data['stripe']['stripe_currency']
        ));
        $this->load->library('paypal_lib', array(
            'clientId'     		=> $this->page_data['paypal']['clientId'],
            'clientSecret' 		=> $this->page_data['paypal']['clientSecret']
        ));
    }

    public function index() {
        $data 			= $this->ServiceModel->serviceList();
        $gcategories 	= $this->GalleryModel->listCat();
        $galleryImages 	= $this->GalleryModel->listGallery();
        $themeViewData  = array_merge(
            $this->page_data,
            array(
                'serviceList' 				=> $data,
                'gcategories'				=> $gcategories,
                'galleryImages'				=> $galleryImages,
                'userinfo'					=> $this->page_data['user']
            )
        );
        $theme_view = $this->page_data['theme_view'];
        $theme_view('booking', $themeViewData);
    }

    public function api_get_services(){
        echo json_encode($this->ServiceModel->serviceList());
    }

    public function get_all_orders(){

        $orders = $this->BookingModel->getPosData();
        foreach($orders as $key => $value)
        {
            if(empty($value['agentName']) || $value['agentName'] === '' || $value['agentName'] === null)
            {
                $orders[$key]['agentName'] = 'Any Agent';
            }
            $to_update = [
                'pos' 	=> 1,
            ];
            $this->BookingModel->updatePosData($value['id'], $to_update);
        }

        if(!$orders){
            $orders = array();
        }

        echo json_encode($orders);
    }
}
