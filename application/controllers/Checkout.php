<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//sumair asim
class Checkout extends CI_Controller
{

    private $page_data;

    public function __construct()
    {
        parent::__construct();
        $this->page_data = $this->MainModel->pageData();
        $this->load->model('StripeModel');
        $this->load->model('OrderModel');
        $this->page_data['stripe'] 		= $this->OrderModel->getStripe();
        $this->StripeModel 				= $this->StripeModel->get();
        $this->load->model('Products_model', 'Products');

        $this->load->library('stripe_lib', array(
            'stripe_api_key'     		=> $this->page_data['stripe']['stripe_api_key'],
            'stripe_publishable_key' 	=> $this->page_data['stripe']['stripe_publishable_key'],
            'stripe_currency' 			=> $this->page_data['stripe']['stripe_currency']
        ));
    }

    public function index($permalink = null)
    {
     //   $theme_view = $this->page_data['theme_view'];
        $themeViewData = array_merge(
            $this->page_data,
            array(
                'cartData' => $this->getCartDetails(),
            )
        );

        $theme_view = $this->page_data['theme_view'];
        $theme_view('checkout', $themeViewData);
    }

    public function add()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {


            $inputs = $this->input->post();


            $cart = $this->session->userdata('cart');
            $cart = $cart ? $cart : [];

            $id = $inputs['product'];
            $qty = (int)$inputs['quantity'];
            $qty = $qty ? $qty : 1;
            $discountItemId = $inputs['QuantityDiscountItem'] ?? NULL;
            $product = $this->Products
                ->find()
                ->where('id', $id)
                ->get()
                ->row_array();

            if ($product) {
                $key = $id;
                if (isset($inputs['attribute'])) {
                    $aids = [];
                    foreach ($inputs['attribute'] as $ProAttributes) {
                        $aids = array_merge($aids, $ProAttributes);
                    }
                    $aids = array_unique($aids);

                    $key .= '-' . implode(',', $aids);
                }

                $key = md5($key);

                if (isset($cart[$key])) {
                    $cart[$key]['quantity'] = $cart[$key]['quantity'] + $qty;
                } else {
                    $cart[$key] = [
                        'product' => $id,
                        'attribute' => !isset($inputs['attribute']) && empty($inputs['attribute']) ? [] : $inputs['attribute'],
                        'quantity' => $qty,
                        'discountItemId' => $discountItemId
                    ];
                }
                $this->session->set_userdata('cart', $cart);
                redirect(base_url('cart'));
            }


            $themeViewData = array_merge(
                $this->page_data,
                array(
                    'cartData' => $this->getCartDetails(),
                )
            );

            $theme_view = $this->page_data['theme_view'];
            $theme_view('cart', $themeViewData);


        }

        redirect(url('/'));
    }

    public function update()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $inputs = $this->input->post();
            $cart = $this->session->userdata('cart');
            $cart = $cart ? $cart : [];

            foreach ($inputs['quantity'] as $key => $qty) {
                if (isset($cart[$key])) {
                    $cart[$key]['quantity'] = $qty;
                }
            }

            $this->session->set_userdata('cart', $cart);
            $this->session->set_flashdata('success', 'Shopping cart updated.');
        }

        redirect(url('/' . $this->vars['cart_url']));
    }

    public function remove()
    {
        $key = $this->input->get('key');
        $cart = $this->session->userdata('cart');

        if ($key && isset($cart[$key])) {
            $item = $cart[$key];

            $product = $this->Products
                ->find()
                ->where('id', $item['product'])
                ->get()
                ->row_array();

            unset($cart[$key]);

            $this->session->set_flashdata('success', '"' . $product['name'] . '" removed.');
            $this->session->set_userdata('cart', $cart);
        }

        redirect(url('/' . $this->vars['cart_url']));
    }

    public function getCartDetails()
    {
        $this->load->model('Product_attributes_vals_model', 'AttributesValues');

        $cart = $this->session->userdata('cart');


        $cart = $cart ? $cart : [];

        $subtotal = $tax = $tax_per = $tax_inc = $nettotal = 0;

        if ($cart) {
            $pids = $aids = [];
            $today = strtotime(date("Y-m-d"));;
            // $today = time();
            foreach ($cart as $c) {
                $pids[] = $c['product'];

            }

            $pids = array_unique($pids);

            $products = $this->Products
                ->find()
                ->select('id, name, slug, image, price, sale_price, sale_start, sale_end')
                ->where_in('id', $pids)
                ->get()
                ->result_array();


            foreach ($cart as $k => $v) {
                foreach ($products as $p) {
                    if ($p['id'] == $v['product']) {
                        $cart[$k]['product_name'] = $p['name'];
                        $cart[$k]['url'] = base_url('/product/' . $p['slug']);
                        $cart[$k]['image'] = $p['image'];
                        $cart[$k]['price'] = $p['price'];

                    }
                }
            }


            $tax_per = isset($this->vars['tax_percentage']) ? (int)$this->vars['tax_percentage'] : 0;
            $tax_inc = isset($this->vars['tax_included']) && $this->vars['tax_included'] == 1 ? 1 : 0;
            $tax = round($subtotal * ($tax_per / 100), 2);
            $nettotal = $tax_inc ? $subtotal : ($subtotal + $tax);
        }

        $result = [
            'items' => $cart,
            'subtotal' => $subtotal,
            'tax' => $tax,
            'tax_percent' => $tax_per,
            'tax_included' => $tax_inc,
            'nettotal' => $nettotal
        ];
        return $result;

    }
}
