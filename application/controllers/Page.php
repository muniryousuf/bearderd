<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	private $page_data;

	public function __construct() {
		parent::__construct();
		$this->load->model('OrderModel');
        $this->load->model('PaypalModel');
        
		$this->page_data            = $this->MainModel->pageData();
        $this->page_data['update']  = $this->MainModel->updates_settings();
		$this->page_data['weekends'] 	= $this->ServiceModel->homepageWeekends();
		$this->page_data['stripe'] 		= $this->OrderModel->getStripe();
		$this->page_data['paypal'] 		= $this->PaypalModel->getPaypal();
	}

	public function index($permalink = null) {
		$this->load->model('Settings/PageModel');

        if($permalink && $page = $this->PageModel->get_page($permalink)) {
            $data = $this->page_data;
            $data['page'] = $page;

            $theme_view = $data['theme_view'];
            $theme_view('page', $data);
        } else
            redirect(base_url());
    }
}
