<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

	function __construct()
    {
        parent::__construct();
        $this->load->model('Shop_orders_model', 'ShopOrders');
        $this->load->model('Shop_order_address_model', 'ShopOrderAddress');
        $this->load->model('Shop_order_products_model', 'ShopOrderProducts');
        $this->load->model('Products_model', 'Products');
        $this->load->model('StripeModel');
        $this->load->model('OrderModel');

        $this->page_data 				= $this->MainModel->pageData();
        $this->page_data['stripe'] 		= $this->OrderModel->getStripe();
        $this->StripeModel 				= $this->StripeModel->get();

        $this->load->library('stripe_lib', array(
            'stripe_api_key'     		=> $this->page_data['stripe']['stripe_api_key'],
            'stripe_publishable_key' 	=> $this->page_data['stripe']['stripe_publishable_key'],
            'stripe_currency' 			=> $this->page_data['stripe']['stripe_currency']
        ));
    }

    public function process()
    {
        $cart   = $this->getCartDetails();

        if($this->input->server('REQUEST_METHOD') == 'POST')
        {

        	$inputs = $this->input->post();
	        $this->form_validation->set_rules('billing[first_name]', 'Voornaam', 'required');
	        $this->form_validation->set_rules('billing[last_name]', 'Achternaam', 'required');
	        $this->form_validation->set_rules('billing[address1]', 'Straat en huisnummer', 'required');
	        $this->form_validation->set_rules('billing[postalcode]', 'Postcode', 'required');
	        $this->form_validation->set_rules('billing[city]', 'Plaats', 'required');
	        $this->form_validation->set_rules('email', 'E-mailadres', 'required|valid_email');
	        $this->form_validation->set_rules('payment_method', 'Payment Method', 'required');
	        $this->form_validation->set_data($inputs);

	        if ($this->form_validation->run() == FALSE)
	        {
	        	$this->session->set_flashdata('error', '<p>Please fix the below mentioned errors:</p>' . validation_errors('- ', '<br />'));
	        	echo validation_errors('- ', '<br />');
	        	exit;

	        	//redirect(url('/' . $this->vars['checkout_url']));
	        }

            $customer_id = 0;
	        // Create order
	        $data = [
	        	'customer_id'    => $customer_id,
                'email'          => $inputs['email'],
                'phone'          => $inputs['phone'],
                'status'         => 0,
                'payment_method' => 'cash',
                'sub_total'      => $cart['subtotal'],
                'tax'            => $cart['tax'],
                'net_total'      => $cart['nettotal'],
                'comments'       => $inputs['comments'],
            ];

            if($this->security->xss_clean($this->input->post('selectPayment')) == 1 && $inputs['stripeToken'] = $this->security->xss_clean($this->input->post('stripeToken'))){// If By Stripe

                // Retrieve stripe token, card and user info from the submitted form data
                $orderID 		= strtoupper(md5((str_replace('.','',uniqid('', true).time()))));
                $totalPrice		= $cart['nettotal'];

                // Send email & token to Stripe
              //  $customer 		= $this->stripe_lib->addCustomer($data['email'], $inputs['stripeToken']);


                //if($customer) {
                    // Charge a credit or a debit card
                    $charge = $this->stripe_lib->createCharge($customer_id, $inputs['billing']['first_name'], $totalPrice, $orderID);

                    print_r($charge);exit;

                    if($charge){
                        // Check whether the charge is successful
                        if($charge['amount_refunded'] == 0 && empty($charge['failure_code']) && $charge['paid'] == 1 && $charge['captured'] == 1){
                            // Transaction details
                            $chargeId 						= $charge['id'];
                            $transactionID 					= $charge['balance_transaction'];
                            $paidAmount 					= $charge['amount'];
                            $paidAmount 					= ($paidAmount/100);
                            $paidCurrency 					= $charge['currency'];
                            $receipt_url 					= $charge['receipt_url'];
                            $payment_status 				= $charge['status'];

                            $formService['orderId'] 		= $chargeId;
                            $formService['payBy'] 			= 'stripe';
                            $formService['paymentStatus'] 	= true;
                            $formService['serviceStatus'] 	= true;

                            $this->ShopOrders->insert($data,false);
                            $order_id = $this->ShopOrders->getLastInsertID();
                            //            // Order billing address
                            $bill_data = $inputs['billing'];
                            $bill_data['type'] = 1;
                            $bill_data['order_id'] = $order_id;
                            $bill_data['country']  = 'UK';
                            $this->ShopOrderAddress->insert($bill_data, false);

                            foreach ($cart['items'] as $pro)
                            {
                                $name = $pro['product_name'];

                                if(isset($pro['attribute']) && !empty($pro['attribute']))
                                {
                                    $name .= ' - ' . implode(', ', $pro['attribute_name']);
                                    $attr = implode(',', $pro['attribute']);
                                }

                                $product_data = [
                                    'order_id'   => $order_id,
                                    'product_id' => $pro['product'],
                                    'name'       => $name,
                                    'attributes' => $attr,
                                    'price'      => $pro['price'],
                                    'quantity'   => $pro['quantity'],
                                ];

                                $this->ShopOrderProducts->insert($product_data, false);
                            }

                            $this->session->set_userdata('cart', '');
                            $this->session->set_flashdata('invMsg','Your Payment & Order has bees Submited');
                            $this->session->set_flashdata('inv_class','alert alert-success');
                            $payment_message = [ 'success' => true, 'msg' => 'You have successfully paid by Stripe!', 'orderid' => $chargeId ];

                            return json_encode(['payment' => $payment_message]);

                        }// Transactions Successfull
                        else{
                            $payment_message = [ 'success' => false, 'msg' => 'Transaction failed!' ];
                            echo json_encode(['payment' => $payment_message]);
                        }// Transaction Failed
                 //   }
                } // Start Credit Card Payment Method
            } else {

                $this->ShopOrders->insert($data,false);
                $order_id = $this->ShopOrders->getLastInsertID();
//            // Order billing address
                $bill_data = $inputs['billing'];
                $bill_data['type'] = 1;
                $bill_data['order_id'] = $order_id;
                $bill_data['country']  = 'UK';
                $this->ShopOrderAddress->insert($bill_data, false);
                // Order products
                foreach ($cart['items'] as $pro)
                {
                    $name = $pro['product_name'];

                    if(isset($pro['attribute']) && !empty($pro['attribute']))
                    {
                        $name .= ' - ' . implode(', ', $pro['attribute_name']);
                        $attr = implode(',', $pro['attribute']);
                    }

                    $product_data = [
                        'order_id'   => $order_id,
                        'product_id' => $pro['product'],
                        'name'       => $name,
                        'attributes' => $attr,
                        'price'      => $pro['price'],
                        'quantity'   => $pro['quantity'],
                    ];

                    $this->ShopOrderProducts->insert($product_data, false);
                }

                $this->session->set_userdata('cart', '');
                $this->session->set_flashdata('invMsg','Your Payment & Order has bees Submited');
                $this->session->set_flashdata('inv_class','alert alert-success');
                $payment_message = [ 'success' => true, 'msg' => 'You have successfully paid by Cash!', 'orderid' => $order_id ];

                echo json_encode($payment_message);

            }
        }
    }

    public function validate_unique_email($email)
    {
        $user = $this->Customers->find()->where('email', $email);

        $result = $user->get()->num_rows();

        return $result == 0 ? true : false;
    }


    public function getCartDetails()
    {
        $this->load->model('Product_attributes_vals_model', 'AttributesValues');

        $cart = $this->session->userdata('cart');


        $cart = $cart ? $cart : [];

        $subtotal = $tax = $tax_per = $tax_inc = $nettotal = 0;

        if ($cart) {
            $pids = $aids = [];
            $today = strtotime(date("Y-m-d"));;
            // $today = time();
            foreach ($cart as $c) {
                $pids[] = $c['product'];

            }

            $pids = array_unique($pids);

            $products = $this->Products
                ->find()
                ->select('id, name, slug, image, price, sale_price, sale_start, sale_end')
                ->where_in('id', $pids)
                ->get()
                ->result_array();


            foreach ($cart as $k => $v) {
                foreach ($products as $p) {
                    if ($p['id'] == $v['product']) {
                        $cart[$k]['product_name'] = $p['name'];
                        $cart[$k]['url'] = base_url('/product/' . $p['slug']);
                        $cart[$k]['image'] = $p['image'];
                        $cart[$k]['price'] = $p['price'];

                    }
                }
            }


            $tax_per = isset($this->vars['tax_percentage']) ? (int)$this->vars['tax_percentage'] : 0;
            $tax_inc = isset($this->vars['tax_included']) && $this->vars['tax_included'] == 1 ? 1 : 0;
            $tax = round($subtotal * ($tax_per / 100), 2);
            $nettotal = $tax_inc ? $subtotal : ($subtotal + $tax);
        }

        $result = [
            'items' => $cart,
            'subtotal' => $subtotal,
            'tax' => $tax,
            'tax_percent' => $tax_per,
            'tax_included' => $tax_inc,
            'nettotal' => $nettotal
        ];
        return $result;

    }
}
