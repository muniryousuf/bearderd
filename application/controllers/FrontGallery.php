<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FrontGallery extends CI_Controller {
	
	private $page_data;

	public function __construct() 
	{
	    		parent::__construct();
	            $this->load->database();
	        	$this->page_data 				= $this->MainModel->pageData();   
                $this->load->helper('text');
                $this->load->model('GalleryModel');
	}

	public function index($permalink = null) 
	{           
	    
	    
	    $gcategories 	= $this->GalleryModel->listCat();
        $galleryImages 	= $this->GalleryModel->listGallery();
    
       
        $themeViewData  = array_merge(
		                        $this->page_data,
								array(
									'gcategories'				=> $gcategories,
									'galleryImages'				=>$galleryImages,
								)
						);
	
	   $theme_view = $this->page_data['theme_view'];
	   
	   $theme_view('our_gallery', $themeViewData);
	
	}
}
?>